package rizzi.graziano.inventariofonetico.controller;

import android.app.Application;
import android.content.Context;
import android.content.Intent;

import java.util.LinkedList;
import java.util.List;

import rizzi.graziano.inventariofonetico.data.DataManager;
import rizzi.graziano.inventariofonetico.data.DataRequestCallback;
import rizzi.graziano.inventariofonetico.data.json.JsonManager;
import rizzi.graziano.inventariofonetico.data.persistence.entity.Letter;
import rizzi.graziano.inventariofonetico.data.to.LetterTO;

public class ApplicationController extends Application {

    public static ApplicationController instance;
    public List<Letter> letters = new LinkedList<>();
    private boolean minTimeSplashGone = false;
    private boolean databaseLoaded = false;
    private int minTimeSplash = 5000;

    @Override
    public void onCreate() {
        instance = this;
        super.onCreate();
        new Thread(()-> {
            try{
                Thread.sleep(minTimeSplash);
            }catch (InterruptedException e){
                e.printStackTrace();
            }finally {
                minTimeSplashGone = true;
                if(databaseLoaded)startMenu();
            }
        }).start();
        DataManager dataManager = DataManager.getInstance();
        dataManager.getLetters(new DataRequestCallback<List<Letter>>() {
            @Override
            public void onSuccess(List<Letter> dataResult) {
                letters = dataResult;
                databaseLoaded  = true;
                if(minTimeSplashGone)startMenu();
            }

            @Override
            public void onFail(Exception e) {

            }
        });
    }

    private void startMenu(){
        Intent intent = new Intent(instance, MenuActivity.class);
        instance.startActivity(intent);
    }



    public static Context getContext(){
        return instance;
    }

    public List<Letter> getLetters() {
        return letters;
    }

    public static ApplicationController getInstance() {
        return instance;
    }
}
