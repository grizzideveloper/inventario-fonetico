package rizzi.graziano.inventariofonetico.controller;

import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

public abstract class BaseActivityController extends AppCompatActivity{

    @Override
    public final void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        onCreateController(savedInstanceState);
        onCreateView();
    }

    protected abstract void onCreateController(@Nullable Bundle savedInstanceState);
    protected abstract void onCreateView();
}
