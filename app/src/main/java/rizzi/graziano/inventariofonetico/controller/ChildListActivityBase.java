package rizzi.graziano.inventariofonetico.controller;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import rizzi.graziano.inventariofonetico.R;
import rizzi.graziano.inventariofonetico.controller.adapter.ChildrenListAdapter;
import rizzi.graziano.inventariofonetico.controller.adapter.Main2Activity;
import rizzi.graziano.inventariofonetico.data.DataManager;
import rizzi.graziano.inventariofonetico.data.DataRequestCallback;
import rizzi.graziano.inventariofonetico.data.persistence.entity.Children;
import rizzi.graziano.inventariofonetico.view.Dialog.AsynchronousProgressDialog;
import rizzi.graziano.inventariofonetico.view.MainActivity;

public class ChildListActivityBase extends AppCompatActivity {

    private Context context;
    protected FloatingActionButton fab;
    protected ChildrenListAdapter childrenListAdapter;
    protected List<Children> childrens;
    private AsynchronousProgressDialog progressDialog = new AsynchronousProgressDialog(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = this;
        setContentView(R.layout.activity_child_list2);
        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, Main2Activity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        progressDialog.showProgress();
        ListView listview = findViewById(R.id.testList);
        childrens = new LinkedList<>();
        childrenListAdapter =  new ChildrenListAdapter(this,childrens,isDeletable()) {
            @Override
            public void onClick(Children children) {
                onChildSelected(children);
            }
        };
        listview.setAdapter(childrenListAdapter);
        DataManager dataManager = DataManager.getInstance();
        dataManager.getChildrens(new DataRequestCallback<List<Children>>() {
            @Override
            public void onSuccess(List<Children> dataResult) {
                childrenListAdapter.setChildrenList(dataResult);
                progressDialog.stopProgress();
            }

            @Override
            public void onFail(Exception e) {
                e.printStackTrace();
                progressDialog.stopProgress();
            }
        });
    }

    protected void onChildSelected(Children children){

    }

    protected boolean isDeletable(){
        return false;
    }
}
