package rizzi.graziano.inventariofonetico.controller;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import rizzi.graziano.inventariofonetico.data.persistence.entity.Children;
import rizzi.graziano.inventariofonetico.view.MainActivity;

public class ChildNewTestActivity extends ChildListActivityBase{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fab.setVisibility(View.GONE);
    }

    @Override
    protected void onChildSelected(Children children) {
        Intent intent = new Intent(this,MainActivity.class);
        intent.putExtra("children",children.getUid());
        startActivity(intent);
    }
}
