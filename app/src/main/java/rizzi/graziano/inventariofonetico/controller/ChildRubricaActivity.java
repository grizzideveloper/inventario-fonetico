package rizzi.graziano.inventariofonetico.controller;
import android.app.Activity;
import android.content.DialogInterface;
import android.support.v4.app.FragmentManager;

import rizzi.graziano.inventariofonetico.data.DataManager;
import rizzi.graziano.inventariofonetico.data.DataRequestCallback;
import rizzi.graziano.inventariofonetico.data.persistence.entity.Children;
import rizzi.graziano.inventariofonetico.view.Dialog.CustomDialogInterface;
import rizzi.graziano.inventariofonetico.view.Dialog.DeleteDialogFragment;

public class ChildRubricaActivity extends ChildListActivityBase{

    private Activity activity = this;

    @Override
    protected void onChildSelected(Children children) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        DeleteDialogFragment deleteDialogFragment = new DeleteDialogFragment();
        deleteDialogFragment.setCustomDialogInterface(new CustomDialogInterface() {
            @Override
            public void onCancel(DialogInterface dialog) {
                dialog.dismiss();
            }

            @Override
            public void onConfirm(DialogInterface dialog) {
                DataManager.getInstance().removeChildren(new DataRequestCallback<Void>() {
                    @Override
                    public void onSuccess(Void dataResult) {
                        activity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                childrenListAdapter.removeChildren(children);
                            }
                        });
                    }

                    @Override
                    public void onFail(Exception e) {
                        e.printStackTrace();
                    }
                },children.getUid());
            }
        });
        deleteDialogFragment.show(fragmentManager,"dialogFragment");
    }

    @Override
    protected boolean isDeletable() {
        return true;
    }
}
