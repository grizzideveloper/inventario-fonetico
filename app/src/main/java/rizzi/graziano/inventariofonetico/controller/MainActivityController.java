package rizzi.graziano.inventariofonetico.controller;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import java.util.LinkedList;
import java.util.List;


import rizzi.graziano.inventariofonetico.controller.adapter.TestListAdapter;
import rizzi.graziano.inventariofonetico.data.DataManager;
import rizzi.graziano.inventariofonetico.data.DataRequestCallback;
import rizzi.graziano.inventariofonetico.data.to.TestTO;


public abstract class MainActivityController extends BaseActivityController{

    private List<TestTO> tests;
    private TestListAdapter testListAdapter;
    private int childrenSelected = 0;
    //private Handler handler;


    @Override
    protected final void onCreateController(@Nullable Bundle savedInstanceState) {
        childrenSelected = getIntent().getIntExtra("children",0);
        this.tests  = new LinkedList<>();
        this.testListAdapter = new TestListAdapter(this,tests){
            @Override
            public void onItemSelected(int position) {
                onTestSelect(position,tests.get(position));
            }
        };
        DataManager dataManager = DataManager.getInstance();
        dataManager.getTests(new DataRequestCallback<List<TestTO>>() {
            @Override
            public void onSuccess(List<TestTO> dataResult) {
                tests.addAll(dataResult);
                runOnUiThread(()->{
                    testListAdapter.notifyDataSetChanged();
                });
            }

            @Override
            public void onFail(Exception e) {
                e.printStackTrace();
            }
        });
        onCreateView();
    }



    public TestListAdapter getTestListAdapter(){
        return this.testListAdapter;
    }

    public  void onTestSelect(int position,TestTO test){
        Intent intent = new Intent(this,TestActivityController.class);
        intent.putExtra("test",position);
        intent.putExtra("children",childrenSelected);
        startActivity(intent);
    }

}
