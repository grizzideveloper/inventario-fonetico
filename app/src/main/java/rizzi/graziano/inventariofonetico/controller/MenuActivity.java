package rizzi.graziano.inventariofonetico.controller;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import rizzi.graziano.inventariofonetico.R;
import rizzi.graziano.inventariofonetico.view.MainActivity;

public class MenuActivity extends AppCompatActivity {

    private Button rubrica,testSvolti;
    private View newTest;
    private Context context = this;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        newTest = findViewById(R.id.nuovo_test);
        rubrica = findViewById(R.id.rubrica);
        testSvolti = findViewById(R.id.test_svolti);

        newTest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context,ChildNewTestActivity.class);
                startActivity(intent);
            }
        });

        rubrica.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context,ChildRubricaActivity.class);
                startActivity(intent);
            }
        });

        testSvolti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context,ChildResultActivity.class);
                startActivity(intent);
            }
        });
    }
}
