package rizzi.graziano.inventariofonetico.controller;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.WorkSource;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.view.View;
import android.widget.Button;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;

import rizzi.graziano.inventariofonetico.R;
import rizzi.graziano.inventariofonetico.controller.adapter.WrongWordsAdapter;
import rizzi.graziano.inventariofonetico.data.DataManager;
import rizzi.graziano.inventariofonetico.data.DataRequestCallback;
import rizzi.graziano.inventariofonetico.data.persistence.entity.Children;
import rizzi.graziano.inventariofonetico.data.persistence.entity.Letter;
import rizzi.graziano.inventariofonetico.data.persistence.entity.Problem;
import rizzi.graziano.inventariofonetico.data.persistence.entity.Result;
import rizzi.graziano.inventariofonetico.data.persistence.entity.Test;
import rizzi.graziano.inventariofonetico.data.persistence.entity.TestDone;
import rizzi.graziano.inventariofonetico.data.persistence.entity.Word;
import rizzi.graziano.inventariofonetico.data.persistence.entity.WrongWord;
import rizzi.graziano.inventariofonetico.data.to.TestTO;

public class ResultActivityController extends BaseActivityController {

    private HashMap<Integer,Word> wordHashMap = new HashMap<>();
    private HashMap<Integer,Result> resultHashMap = new HashMap<>();
    private HashMap<Integer,Letter> letterHashMap = new HashMap<>();;
    private HashMap<Integer,Problem> problemHashMap = new HashMap<>();;
    private TestTO test;
    private Children children;
    private TestDone testDone;

    private DataManager dataManager;
    private TextView nomeBambino,dataTest,titolo,livello;

    private TextView textView;
    private Context context = this;

    private int counter = 0;

    private ExpandableListView expandableListView;
    private FloatingActionButton floatingActionButton;

    private Button inviaReuslt;

    private String testo;

    public static final String TEST_DONE_ID = "TEST_DONE_ID";

    @Override
    protected void onCreateController(@Nullable Bundle savedInstanceState) {
        setContentView(R.layout.result_controller_layout);
        expandableListView = findViewById(R.id.wrong_words_list);
        nomeBambino = findViewById(R.id.nome_bambino);
        dataTest = findViewById(R.id.data_test);
        livello = findViewById(R.id.livello);
        titolo = findViewById(R.id.titolo);
        floatingActionButton =  findViewById(R.id.floating_button_end);
        dataManager = DataManager.getInstance();
        inviaReuslt = findViewById(R.id.send_result_email);
        int testDoneId = getIntent().getExtras().getInt(TEST_DONE_ID,1);
        counter = 7;

        inviaReuslt.setOnClickListener((View v) ->{
            Intent intent = new Intent(Intent.ACTION_SEND);
            intent.setType("text/html");
            intent.putExtra(Intent.EXTRA_EMAIL, new String[]{children.getEmail()});
            intent.putExtra(Intent.EXTRA_SUBJECT, "Report");
            intent.putExtra(Intent.EXTRA_TEXT, testo);
            startActivity(Intent.createChooser(intent, "Send Email"));
        });

        textView = findViewById(R.id.textView2);
        floatingActionButton.setOnClickListener((View v) -> {
                finish();
        });

        dataManager.getLetters(new DataRequestCallback<List<Letter>>() {
            @Override
            public void onSuccess(List<Letter> dataResult) {

                for (Letter l:dataResult) {
                    letterHashMap.put(l.getUid(),l);
                }
                decrement();
            }

            @Override
            public void onFail(Exception e) {
                e.printStackTrace();
            }
        });
        
        dataManager.getProblems(new DataRequestCallback<List<Problem>>() {
            @Override
            public void onSuccess(List<Problem> dataResult) {

                for (Problem p:dataResult) {
                    problemHashMap.put(p.getUid(),p);
                }
                decrement();
            }

            @Override
            public void onFail(Exception e) {
                e.printStackTrace();
            }
        });
        
        dataManager.getAllWords(new DataRequestCallback<List<Word>>() {
            @Override
            public void onSuccess(List<Word> dataResult) {

                for (Word w:dataResult) {
                    wordHashMap.put(w.getUid(),w);
                }
                decrement();
            }

            @Override
            public void onFail(Exception e) {
                e.printStackTrace();
            }
        });


        dataManager.getTestDone(new DataRequestCallback<TestDone>() {
            @Override
            public void onSuccess(TestDone dataResult) {
                decrement();
                testDone = dataResult;
                dataManager.getResultForTestDone(new DataRequestCallback<List<Result>>() {
                    @Override
                    public void onSuccess(List<Result> dataResult) {

                        for (Result r:dataResult) {
                            resultHashMap.put(r.getUid(),r);
                        }
                        decrement();
                    }

                    @Override
                    public void onFail(Exception e) {
                        e.printStackTrace();
                    }
                },dataResult.getUid());

                dataManager.getTest(new DataRequestCallback<TestTO>() {
                    @Override
                    public void onSuccess(TestTO dataResult) {

                        test = dataResult;
                        decrement();
                    }

                    @Override
                    public void onFail(Exception e) {
                        e.printStackTrace();
                    }
                },dataResult.getTestID());

                dataManager.getChildren(new DataRequestCallback<Children>() {
                    @Override
                    public void onSuccess(Children dataResult) {
                        children = dataResult;
                        decrement();
                    }

                    @Override
                    public void onFail(Exception e) {

                    }
                },dataResult.getChildrenID());
            }

            @Override
            public void onFail(Exception e) {
                e.printStackTrace();
            }
        }, testDoneId);
    }

    @Override
    protected void onCreateView() {

    }


    private void decrement(){
        counter--;
        if(counter == 0){
            onEndLoadingData();
        }
    }

    private void onEndLoadingData(){
        testo = "Test: ";
        testo += test.getTitle();
        testo += "\n\nLivello: " + test.getLevel();
        testo += "\n\nDescrizione: " + test.getDescription();
        HashSet<Word> wordsSbagliate = new HashSet<>();
        for (Result r:resultHashMap.values()) {
            wordsSbagliate.add(wordHashMap.get(r.getWordID()));
        }

        testo += "\n\nParole errate: \n";
        final List<WrongWord> wrongWordList = new LinkedList<>();
        for (Word w:wordsSbagliate) {
            WrongWord wrongWord = new WrongWord();
            wrongWord.setWord(w.getWordText());
            List<String> errors = new LinkedList<>();
            wrongWord.setErrors(errors);
            testo += "\n-" + w.getWordText() + "\n";
            for(Result r:resultHashMap.values()){
                if(r.getWordID() == w.getUid()){
                    Letter letterp = letterHashMap.get(r.getLetterID());
                    Letter letters = letterHashMap.get(r.getReplaceLetterID());
                    Problem p = problemHashMap.get(r.getProblemID());
                    if(r.getProblemID() == 1){
                        String error = p.getDesc() /*+ " " + r.getIndex()*/ + " " + letterp.getLetter()  + " -> " + letters.getLetter() + "\n";
                        testo += error;
                        errors.add(error);
                    }else {
                        String error =  p.getDesc() /*+ " " + r.getIndex()*/ + " " + letterp.getLetter() + "\n";
                        testo += error;
                        errors.add(error);
                    }
                }
            }
            wrongWordList.add(wrongWord);
        }
        final String tx = testo;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                nomeBambino.setText(children.getName() + " " + children.getSurname());
                dataTest.setText(testDone.getDate());
                titolo.setText(test.getTitle());
                livello.setText("Livello " + test.getLevel());
                expandableListView.setAdapter(new WrongWordsAdapter(wrongWordList,context));
            }
        });
    }
}
