package rizzi.graziano.inventariofonetico.controller;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.PagerAdapter;
import android.support.v7.app.AlertDialog;
import android.view.View;

import junit.framework.TestSuite;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import rizzi.graziano.inventariofonetico.R;
import rizzi.graziano.inventariofonetico.controller.adapter.CustomTableKeyboardAdapter;
import rizzi.graziano.inventariofonetico.controller.adapter.TestPagesAdapter;
import rizzi.graziano.inventariofonetico.data.DataManager;
import rizzi.graziano.inventariofonetico.data.DataRequestCallback;
import rizzi.graziano.inventariofonetico.data.persistence.entity.Letter;
import rizzi.graziano.inventariofonetico.data.persistence.entity.Result;
import rizzi.graziano.inventariofonetico.data.persistence.entity.Test;
import rizzi.graziano.inventariofonetico.data.persistence.entity.TestDone;
import rizzi.graziano.inventariofonetico.data.to.LetterTO;
import rizzi.graziano.inventariofonetico.data.to.ResultTO;
import rizzi.graziano.inventariofonetico.data.to.TestTO;
import rizzi.graziano.inventariofonetico.view.CustomViewPager;
import rizzi.graziano.inventariofonetico.view.MyKeyboardView;
import rizzi.graziano.inventariofonetico.view.TestPageView;

public class TestActivityController extends BaseActivityController {

    private LetterTO selectedLetter = null;
    private TestPagesAdapter pagerAdapter = null;
    private Context context = this;
    private FloatingActionButton floatingActionButton;

    @Override
    protected void onCreateController(@Nullable Bundle savedInstanceState) {

    }

    @Override
    protected void onCreateView() {
        setContentView(R.layout.activity_test_pager);
        final CustomViewPager viewPager = findViewById(R.id.viewPager);
        final MyKeyboardView myKeyboardView = findViewById(R.id.mykeyboard);
        floatingActionButton = findViewById(R.id.floating_button);
        floatingActionButton.setVisibility(View.GONE);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for(int i = viewPager.getCurrentItem() ; i < pagerAdapter.getCount() - 1;i++){
                    viewPager.setCurrentItem(viewPager.getCurrentItem() + 1,true);
                }
            }
        });
        myKeyboardView.setAdapter(new CustomTableKeyboardAdapter(){
            @Override
            public void onCellClick(Letter object, int row, int col) {
                ResultTO result = new ResultTO();
                result.setIndex(selectedLetter.getIndex());
                result.setLetterID(selectedLetter.getId());
                result.setTestDoneID(0);
                result.setTestID(pagerAdapter.getTest().getId());
                result.setWordID(pagerAdapter.getCurrentWordID());
                result.setLetter(object.getText());
                selectedLetter.setResult(result);
                myKeyboardView.setVisibility(View.GONE);
                result.setProblemID(1);//Sostituzione
                result.setReplaceLetterID(object.getUid());
            }

            @Override
            public void onLongCellClick(Letter object, int row, int col,int problem) {

            }

            @Override
            public void notifyLongClick(int row, int col,int problem) {

            }

            @Override
            public String getImageName() {
                return null;
            }

            @Override
            public void onClearButtonClick() {
                selectedLetter.clearResult();
                myKeyboardView.setVisibility(View.GONE);
            }

            @Override
            public void onCancelingButton() {
                myKeyboardView.setVisibility(View.GONE);
            }
        });

        int testIndex = getIntent().getIntExtra("test",0);
        int children = getIntent().getIntExtra("children",1);
        DataManager dataManager = DataManager.getInstance();
        dataManager.getTests(new DataRequestCallback<List<TestTO>>() {
            @Override
            public void onSuccess(List<TestTO> dataResult) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        pagerAdapter = new TestPagesAdapter(dataResult.get(testIndex)){
                            @Override
                            public void OnClick(int test, LetterTO letterTO, TestPageView testPageView) {
                                selectedLetter = letterTO;
                                myKeyboardView.show(testPageView::drow);
                            }

                            @Override
                            public void OnLongClick(int test, LetterTO letterTO,int problem) {
                                selectedLetter = letterTO;
                                ResultTO result = new ResultTO();
                                result.setIndex(selectedLetter.getIndex());
                                result.setLetterID(selectedLetter.getId());
                                result.setTestDoneID(0);
                                result.setTestID(pagerAdapter.getTest().getId());
                                result.setWordID(pagerAdapter.getCurrentWordID());
                                result.setLetter("PROB " + problem);
                                result.setProblemID(problem);
                                result.setReplaceLetterID(1);
                                selectedLetter.setResult(result);
                            }

                            @Override
                            public void onEndTestNew() {
                                TestDone testDone = new TestDone();
                                testDone.setChildrenID(children);
                                testDone.setDate(new Date().toString());
                                testDone.setTestID(pagerAdapter.getTest().getId());
                                dataManager.insertTestDone(new DataRequestCallback<Long>() {
                                    @Override
                                    public void onSuccess(Long dataResult) {
                                        int id = dataResult.intValue();
                                        TestTO t = pagerAdapter.getTest();
                                        LinkedList<ResultTO> results = new LinkedList<>();
                                        for(int i = 0;i < t.getWords().size();i++){
                                            for(int j = 0; j < t.getWords().get(i).getLetters().size();j++){
                                                if(t.getWords().get(i).getLetters().get(j).getResult() != null){
                                                    t.getWords().get(i).getLetters().get(j).getResult().setTestDoneID(dataResult.intValue());
                                                    results.add(t.getWords().get(i).getLetters().get(j).getResult());
                                                }
                                            }
                                        }
                                        dataManager.insertResult(new DataRequestCallback<Void>() {
                                            @Override
                                            public void onSuccess(Void dataResult) {
                                                Intent intent = new Intent(context,ResultActivityController.class);
                                                intent.putExtra(ResultActivityController.TEST_DONE_ID,id);
                                                context.startActivity(intent);
                                                finish();
                                            }

                                            @Override
                                            public void onFail(Exception e) {
                                                e.printStackTrace();
                                            }
                                        },results);
                                    }

                                    @Override
                                    public void onFail(Exception e) {
                                        e.printStackTrace();
                                    }
                                },testDone);
                            }
                        };
                        viewPager.setAdapter(pagerAdapter);
                        viewPager.addOnPageChangeListener(pagerAdapter);
                    }
                });
            }

            @Override
            public void onFail(Exception e) {
                e.printStackTrace();
            }
        });
    }

    @Override
    public void onBackPressed() {
        showCloseDialog();
    }

    private void showCloseDialog(){
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(context, android.R.style.Theme_Material_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(context);
        }
        builder.setTitle("Esci")
                .setMessage("Vuoi uscire dal test senza completarlo?")
                .setPositiveButton(R.string.si, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                        TestActivityController.this.finish();
                    }
                })
                .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                        dialog.dismiss();
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }
}
