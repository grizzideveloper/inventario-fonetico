package rizzi.graziano.inventariofonetico.controller;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListView;

import java.util.List;

import rizzi.graziano.inventariofonetico.R;
import rizzi.graziano.inventariofonetico.controller.adapter.TestDoneListAdapter;
import rizzi.graziano.inventariofonetico.data.DataManager;
import rizzi.graziano.inventariofonetico.data.DataRequestCallback;
import rizzi.graziano.inventariofonetico.data.persistence.entity.TestDone;

public class TestDoneActivity extends AppCompatActivity {

    private Context context = this;
    private ListView listView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_done);
        listView = findViewById(R.id.testdone_list);
        TestDoneListAdapter testDoneListAdapter = new TestDoneListAdapter(){
            @Override
            protected void onTestDoneClick(TestDone testDone) {
                Intent intent = new Intent(context,ResultActivityController.class);
                intent.putExtra(ResultActivityController.TEST_DONE_ID,testDone.getUid());
                context.startActivity(intent);
            }
        };
        listView.setAdapter(testDoneListAdapter);
        DataManager.getInstance().getTestsDone(new DataRequestCallback<List<TestDone>>() {
            @Override
            public void onSuccess(List<TestDone> dataResult) {
                testDoneListAdapter.setTestDone(dataResult);
            }

            @Override
            public void onFail(Exception e) {
                e.printStackTrace();
            }
        });
    }


}
