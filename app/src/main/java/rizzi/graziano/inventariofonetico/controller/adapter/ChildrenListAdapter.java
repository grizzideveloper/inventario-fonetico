package rizzi.graziano.inventariofonetico.controller.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.LinkedList;
import java.util.List;

import rizzi.graziano.inventariofonetico.R;
import rizzi.graziano.inventariofonetico.data.persistence.entity.Children;


import static rizzi.graziano.inventariofonetico.controller.ApplicationController.getContext;

public abstract class ChildrenListAdapter extends BaseAdapter {

    private List<Children> childrenList;
    private Context context;
    private boolean deletableRow;

    public ChildrenListAdapter(Context context,List<Children> childrenList,boolean deletableRow) {
        this.context = context;
        if(childrenList != null){
            this.childrenList = childrenList;
        }else {
            this.childrenList  = new LinkedList<>();
        }
        this.deletableRow = deletableRow;
    }

    @Override
    public int getCount() {
        return childrenList.size();
    }

    @Override
    public Children getItem(int position) {
        return childrenList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.problem_dialog_row, parent, false);
        }
        ImageButton imageButton = convertView.findViewById(R.id.delete_row_button);
        if(!deletableRow)imageButton.setVisibility(View.GONE);
        TextView textView = convertView.findViewById(R.id.problem_row_text);
        textView.setText(getItem(position).getName() + " " + getItem(position).getSurname());
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ChildrenListAdapter.this.onClick(getItem(position));
            }
        });
        return convertView;
    }

    public void setChildrenList(List<Children> childrenList) {
        if(childrenList != null){
            this.childrenList = childrenList;
            ((Activity)context).runOnUiThread(()-> notifyDataSetChanged());
        }
    }
    public void removeChildren(Children children){
        childrenList.remove(children);
        notifyDataSetChanged();
    }

    public abstract void onClick(Children children);
}
