package rizzi.graziano.inventariofonetico.controller.adapter;

import rizzi.graziano.inventariofonetico.data.to.LetterTO;

public interface CustomTableAdapter<T> {
    public int getRowCount();
    public int getColumnCount(int forRow);
    public String getText(int row, int col);
    public String getStyle(int row,int col);
    public void onCellClick(T object, int row, int col);
    public void onLongCellClick(T object,int row,int col,int problem);
    public void notifyClick(int row, int col);
    public void notifyLongClick(int row, int col,int problem);
    public String getImageName();
}
