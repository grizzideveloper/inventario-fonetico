package rizzi.graziano.inventariofonetico.controller.adapter;

import java.util.LinkedList;
import java.util.List;

import rizzi.graziano.inventariofonetico.controller.ApplicationController;
import rizzi.graziano.inventariofonetico.data.DataManager;
import rizzi.graziano.inventariofonetico.data.DataRequestCallback;
import rizzi.graziano.inventariofonetico.data.persistence.entity.Letter;

public abstract class  CustomTableKeyboardAdapter implements CustomTableAdapter<Letter> {

    private List<Letter> letters = new LinkedList<Letter>();


    public CustomTableKeyboardAdapter(){
        super();
        /*DataManager dataManager = DataManager.getInstance();
        dataManager.getLetters(new DataRequestCallback<List<Letter>>() {
            @Override
            public void onSuccess(List<Letter> dataResult) {
                for (int i = 0;i < dataResult.size();){
                    if (!dataResult.get(i).isAvKeyboard()){
                        dataResult.remove(i);
                    }else{
                        i++;
                    }
                }
                letters.addAll(dataResult);
            }

            @Override
            public void onFail(Exception e) {

            }
        });*/
        letters = ApplicationController.getInstance().letters;
        for (int i = 0;i < letters.size();){
            if (!letters.get(i).isAvKeyboard()){
                letters.remove(i);
            }else{
                i++;
            }
        }
    }

    @Override
    public int getColumnCount(int forRow) {
        int columnCount = 0;
        /*switch (forRow){
            case 0:
                columnCount = 6;
                break;
            case 1:
                columnCount = 6;
                break;
            case 2:
                columnCount = 6;
                break;
            case 3:
                columnCount = 6;
                break;
            case 4:
                columnCount = 5;
        }*/
        int nLetters = letters.size();
        int count = ((forRow + 1) * 6);
        if (count < nLetters)columnCount = 6;
        else {
            columnCount = nLetters - (count - 6);
        }
        return columnCount;
    }

    @Override
    public int getRowCount() {
        return (letters.size() / 6) + 1;
    }

    public int getCellCount(){
       return letters.size();
    }

    @Override
    public String getText(int row, int col) {
        int position = 0;
        for(int i = 0;i <= (row - 1);i++){
            position += getColumnCount(i);
        }
        position += col;
        String letterToReturn = "0";
        if(position < letters.size()) {
            letterToReturn = letters.get(position).getText();
        }
        return letterToReturn;//"" + (char)(startLetterIndex + position);
    }

    private Letter getLetter(int row, int col) {
        int position = 0;
        for(int i = 0;i <= (row - 1);i++){
            position += getColumnCount(i);
        }
        position += col;
        Letter letterToReturn = null;
        if(position < letters.size())letterToReturn = letters.get(position);
        return letterToReturn;//"" + (char)(startLetterIndex + position);
    }


    @Override
    public final void notifyClick(int row, int col){
        onCellClick(getLetter(row,col),row,col);
    }




    @Override
    public String getStyle(int row, int col) {
        return "";
    }


    public abstract void onClearButtonClick();
    public abstract void onCancelingButton();

}
