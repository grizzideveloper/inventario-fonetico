
package rizzi.graziano.inventariofonetico.controller.adapter;

import android.app.DatePickerDialog;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CalendarView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import rizzi.graziano.inventariofonetico.R;
import rizzi.graziano.inventariofonetico.data.DataManager;
import rizzi.graziano.inventariofonetico.data.DataRequestCallback;
import rizzi.graziano.inventariofonetico.data.persistence.entity.Children;

public class Main2Activity extends AppCompatActivity {

    private EditText nome,cognome,codiceFiscale,email;
    private TextView dataNascita,salva;
    private CalendarView calendarView;
    private  DataManager dataManager = DataManager.getInstance();
    private int day,month,year;
    protected RadioButton radioButton;
    private boolean dateSelected;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_child);
        nome = findViewById(R.id.editText);
        cognome = findViewById(R.id.editText2);
        email = findViewById(R.id.editText3);
        codiceFiscale = findViewById(R.id.editText4);
        dataNascita = findViewById(R.id.born_date);
        salva = findViewById(R.id.salva);
        calendarView = findViewById(R.id.calendar);
        calendarView.setDate(System.currentTimeMillis(),false,true);
        radioButton = findViewById(R.id.radio_button_sex);

        calendarView.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(@NonNull CalendarView view, int year, int month, int dayOfMonth) {
                Main2Activity.this.day = dayOfMonth;
                Main2Activity.this.month = month;
                Main2Activity.this.year = year;
                dataNascita.setText(dayOfMonth + "/" + (month + 1) + "/" + year);
                dateSelected = true;
            }
        });

        salva.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!nome.getText().toString().equals("") &&
                        !cognome.getText().toString().equals("") &&
                        /*!codiceFiscale.getText().toString().equals("") &&*/
                        !email.getText().toString().equals("") &&
                        dateSelected){
                    Children children = new Children();
                    children.setName(nome.getText().toString());
                    children.setCode("");//codiceFiscale.getText().toString());
                    children.setSurname(cognome.getText().toString());
                    children.setEmail(email.getText().toString());
                    children.setBornDate(dataNascita.getText().toString());
                    if(radioButton.isChecked())
                        children.setMale(true);
                    else children.setMale(false);
                    DataManager.getInstance().insertNewChildren(new DataRequestCallback<Void>() {
                        @Override
                        public void onSuccess(Void dataResult) {
                            Main2Activity.this.finish();
                        }

                        @Override
                        public void onFail(Exception e) {

                        }
                    },children);
                }
            }
        });

        Calendar c = Calendar.getInstance();
        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day = c.get(Calendar.DAY_OF_MONTH);



        dataNascita.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog  startTime = new DatePickerDialog(Main2Activity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        String myDate = year+"/"+(month+1)+"/"+dayOfMonth;
                        dataNascita.setText(dayOfMonth + "/" + (month + 1) + "/" + year);
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
                        dateSelected = true;
                        try {
                            Date date = sdf.parse(myDate);
                            calendarView.setDate(date.getTime());
                        }catch (Exception e){

                        }
                    }
                },year,month,day);
                startTime.show();
            }
        });
    }
}
