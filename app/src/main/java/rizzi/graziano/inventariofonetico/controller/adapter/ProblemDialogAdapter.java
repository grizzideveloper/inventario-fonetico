package rizzi.graziano.inventariofonetico.controller.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import rizzi.graziano.inventariofonetico.R;
import rizzi.graziano.inventariofonetico.data.persistence.entity.Problem;
import rizzi.graziano.inventariofonetico.data.persistence.entity.Test;

public class ProblemDialogAdapter extends ArrayAdapter<Problem> {

    public ProblemDialogAdapter(@NonNull Context context, int resource) {
        super(context, resource);
    }

    public ProblemDialogAdapter(@NonNull Context context, int resource, int textViewResourceId) {
        super(context, resource, textViewResourceId);
    }

    public ProblemDialogAdapter(@NonNull Context context, int resource, @NonNull Problem[] objects) {
        super(context, resource, objects);
    }

    public ProblemDialogAdapter(@NonNull Context context, int resource, int textViewResourceId, @NonNull Problem[] objects) {
        super(context, resource, textViewResourceId, objects);
    }

    public ProblemDialogAdapter(@NonNull Context context, int resource, @NonNull List<Problem> objects) {
        super(context, resource, objects);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.problem_dialog_row_2, parent, false);
        }
        TextView textView = convertView.findViewById(R.id.problem_row_text);
        textView.setText(getItem(position).getDesc());

        return convertView;
    }

}
