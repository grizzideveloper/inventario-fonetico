package rizzi.graziano.inventariofonetico.controller.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.LinkedList;
import java.util.List;

import rizzi.graziano.inventariofonetico.R;
import rizzi.graziano.inventariofonetico.data.persistence.entity.TestDone;

import static rizzi.graziano.inventariofonetico.controller.ApplicationController.getContext;

public abstract class TestDoneListAdapter extends BaseAdapter {

    private List<TestDone> results = new LinkedList<>();
    private Context context;

    public TestDoneListAdapter() {
    }

    @Override
    public int getCount() {
        return results.size();
    }

    @Override
    public TestDone getItem(int position) {
        return results.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.problem_dialog_row, parent, false);
        }
        TextView textView = convertView.findViewById(R.id.problem_row_text);
        textView.setText(getItem(position).getDate());
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onTestDoneClick(getItem(position));
            }
        });
        return convertView;
    }

    public void setTestDone(List<TestDone> testDoneList){
        this.results.clear();
        this.results.addAll(testDoneList);
        notifyDataSetChanged();
    }

    protected abstract void onTestDoneClick(TestDone testDone);
}
