package rizzi.graziano.inventariofonetico.controller.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.LinkedList;
import java.util.List;

import rizzi.graziano.inventariofonetico.data.to.TestTO;
import rizzi.graziano.inventariofonetico.view.TestListViewRow;

public abstract class TestListAdapter  extends BaseAdapter{

    private Context context;
    private List<TestTO> tests;

    private List<TestListViewRow> rows;



    public TestListAdapter(Context context,List<TestTO> tests){
        this.context = context;
        this.tests = tests;
        this.rows = new LinkedList<>();
    }

    @Override
    public int getCount() {
        return tests.size(); //returns total of items in the list
    }

    @Override
    public TestTO getItem(int position) {
        return tests.get(position); //returns list item at the specified position
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // inflate the layout for each list row
        if (convertView == null) {
            TestListViewRow row = new TestListViewRow(context,parent,position){
                @Override
                public void onClick(View v) {
                    onItemSelected(this.getPosition());
                }
            };
            rows.add(row);
            convertView = row.getRow();
        }

        TestListViewRow row = rows.get(position);
        TestTO test = tests.get(position);

        row.setDesc(test.getDescription());
        row.setLevel(test.getLevel());
        row.setTitle(test.getTitle());
        row.setPosition(position);
        // get the TextView for item name and item description
        //TextView textViewItemName = (TextView)convertView.findViewById(R.id.text_view_item_name);

        // returns the view for the current row
        return convertView;
    }

    public abstract void onItemSelected(int position);
}
