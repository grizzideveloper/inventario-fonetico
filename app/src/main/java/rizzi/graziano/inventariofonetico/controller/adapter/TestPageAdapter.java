package rizzi.graziano.inventariofonetico.controller.adapter;

import rizzi.graziano.inventariofonetico.data.to.LetterTO;
import rizzi.graziano.inventariofonetico.data.to.WordTO;

public abstract class TestPageAdapter implements CustomTableAdapter<LetterTO> {

    private WordTO wordTO;
    private int numberOfCellForLine = 7;

    public TestPageAdapter(WordTO word){
        this.wordTO = word;
    }


    public String getImageName(){
        return wordTO.getImage_name();
    }

    public int getRowCount(){
        int letterCount = wordTO.getLetters().size();
        return (letterCount/numberOfCellForLine) + 1;
    }

    public int getColumnCount(int forRow){
        int columnCount = numberOfCellForLine;
        if(forRow == getRowCount() - 1){
            columnCount = wordTO.getLetters().size() - ((forRow) * numberOfCellForLine);
        }
        return  columnCount;
    }

    @Override
    public String getText(int row, int col){
        return wordTO.getLetters().get((row*numberOfCellForLine) + col).getText();
    }

    @Override
    public final void notifyClick(int row, int col){
        onCellClick(wordTO.getLetters().get((row*numberOfCellForLine) + col),row,col);
    }

    @Override
    public String getStyle(int row, int col) {
        if (wordTO.getLetters().get((row*numberOfCellForLine) + col).getResult() != null )
            return wordTO.getLetters().get((row*numberOfCellForLine) + col).getResult().getLetter();
        else return "";
    }


    @Override
    public final void notifyLongClick(int row, int col,int problem){
        onLongCellClick(wordTO.getLetters().get((row*numberOfCellForLine) + col),row,col,problem);
    }


}
