package rizzi.graziano.inventariofonetico.controller.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.Toast;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import rizzi.graziano.inventariofonetico.controller.ApplicationController;
import rizzi.graziano.inventariofonetico.data.persistence.entity.TestDone;
import rizzi.graziano.inventariofonetico.data.persistence.entity.Word;
import rizzi.graziano.inventariofonetico.data.to.LetterTO;
import rizzi.graziano.inventariofonetico.data.to.TestTO;
import rizzi.graziano.inventariofonetico.data.to.WordTO;
import rizzi.graziano.inventariofonetico.view.BaseTestPage;
import rizzi.graziano.inventariofonetico.view.TestPageResultView;
import rizzi.graziano.inventariofonetico.view.TestPageView;

import static rizzi.graziano.inventariofonetico.controller.ApplicationController.getContext;

public abstract class TestPagesAdapter extends PagerAdapter implements ViewPager.OnPageChangeListener {

    private TestTO test;
    private int currentPosition = 0;

    public TestTO getTest() {
        return test;
    }

    public TestPagesAdapter(TestTO test){
        super();
        this.test = test;
    }

    public int getCurrentWordID(){
        return test.getWords().get(currentPosition).getId();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @Override
    public int getCount() {
        return test.getWords().size() + 1;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        if(position == getCount() -1){
            TestPageResultView resultView = new TestPageResultView(getContext(),test){
                @Override
                public void onEndTest() {
                    onEndTestNew();
                }
            };
            container.addView(resultView);
            return resultView;
        }else{
            final int index = position;
            TestPageView testPageView = new TestPageView(container.getContext());
            testPageView.setPageAdapter(new TestPageAdapter(test.getWords().get(index)){
                @Override
                public void onCellClick(LetterTO letterTO,int row, int col) {
                    OnClick(index,letterTO,testPageView);
                    testPageView.drow();
                }

                @Override
                public void onLongCellClick(LetterTO object, int row, int col, int problem) {
                    OnLongClick(index,object,problem);
                    testPageView.drow();
                }
            });
            testPageView.reloadBitmap();
            container.addView(testPageView);
            return testPageView;
        }
        /*
        if(position == getCount() -1){
            pageViews.get(position).updateUI();
        }
        if(position - 6 > 0){
            pageViews.get(position - 6).releaseBitmap();
        }
        if(position + 6 < pageViews.size() - 1){
            pageViews.get(position + 6).releaseBitmap();
        }
        pageViews.get(position).reloadBitmap();
        container.addView(pageViews.get(position));
        return pageViews.get(position);//Questo meccanismo permette di non ricreare ogni volta le varie pagine e di aggiornare l'aspetto del singolo tasto*/
    }

    public abstract void onEndTestNew();

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return test.getTitle();
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View)object);

    }

    public abstract void OnClick(int test,LetterTO letterTO,TestPageView testPageView);
    public abstract void OnLongClick(int test,LetterTO letterTO,int problem);

    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        currentPosition = position;
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}
