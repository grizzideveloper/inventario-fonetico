package rizzi.graziano.inventariofonetico.controller.adapter;

import android.content.Context;
import android.database.DataSetObserver;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListAdapter;
import android.widget.TextView;

import java.util.List;

import rizzi.graziano.inventariofonetico.R;
import rizzi.graziano.inventariofonetico.data.persistence.entity.WrongWord;

public class WrongWordsAdapter extends BaseExpandableListAdapter {

    private List<WrongWord> wrongWords;
    private Context context;

    public WrongWordsAdapter(List<WrongWord> wrongWords,Context context){
        this.wrongWords = wrongWords;
        this.context = context;
    }

    @Override
    public int getGroupCount() {
        return wrongWords.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return wrongWords.get(groupPosition).getErrors().size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return wrongWords.get(groupPosition).getWord();
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return wrongWords.get(groupPosition).getErrors().get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        String headerTitle = (String) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this.context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.list_group, null);
        }

        TextView lblListHeader = (TextView) convertView
                .findViewById(R.id.lblListHeader);
        lblListHeader.setTypeface(null, Typeface.BOLD);
        lblListHeader.setText(headerTitle);

        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        final String childText = (String) getChild(groupPosition, childPosition);

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this.context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.list_item, null);
        }

        TextView txtListChild = (TextView) convertView
                .findViewById(R.id.lblListHeader);

        txtListChild.setText(childText);
        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }
}
