package rizzi.graziano.inventariofonetico.data;


import java.sql.DataTruncation;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;

import rizzi.graziano.inventariofonetico.controller.ApplicationController;
import rizzi.graziano.inventariofonetico.data.json.JsonManager;
import rizzi.graziano.inventariofonetico.data.persistence.AppDatabase;
import rizzi.graziano.inventariofonetico.data.persistence.dao.ChildrenDAO;
import rizzi.graziano.inventariofonetico.data.persistence.dao.LetterDao;
import rizzi.graziano.inventariofonetico.data.persistence.dao.LetterXWordDao;
import rizzi.graziano.inventariofonetico.data.persistence.dao.ProblemDAO;
import rizzi.graziano.inventariofonetico.data.persistence.dao.ResultDao;
import rizzi.graziano.inventariofonetico.data.persistence.dao.TestDao;
import rizzi.graziano.inventariofonetico.data.persistence.dao.TestDoneDAO;
import rizzi.graziano.inventariofonetico.data.persistence.dao.WordDao;
import rizzi.graziano.inventariofonetico.data.persistence.dao.WordXTestDao;
import rizzi.graziano.inventariofonetico.data.persistence.entity.Children;
import rizzi.graziano.inventariofonetico.data.persistence.entity.Letter;
import rizzi.graziano.inventariofonetico.data.persistence.entity.LetterXWord;
import rizzi.graziano.inventariofonetico.data.persistence.entity.Problem;
import rizzi.graziano.inventariofonetico.data.persistence.entity.Result;
import rizzi.graziano.inventariofonetico.data.persistence.entity.Test;
import rizzi.graziano.inventariofonetico.data.persistence.entity.TestDone;
import rizzi.graziano.inventariofonetico.data.persistence.entity.Word;
import rizzi.graziano.inventariofonetico.data.persistence.entity.WordXTest;
import rizzi.graziano.inventariofonetico.data.persistence.preference.Preference;
import rizzi.graziano.inventariofonetico.data.to.LetterTO;
import rizzi.graziano.inventariofonetico.data.to.ResultTO;
import rizzi.graziano.inventariofonetico.data.to.TestTO;
import rizzi.graziano.inventariofonetico.data.to.WordTO;

public class DataManager {

    private static DataManager instance;

    private AppDatabase database;
    private TestDao testDao;
    private WordDao wordDao;
    private LetterDao letterDao;
    private WordXTestDao wordXTestDao;
    private LetterXWordDao letterXWordDao;
    private ProblemDAO problemDAO;
    private ChildrenDAO childrenDao;
    private TestDoneDAO testDoneDAO;
    private ResultDao resultDao;

    private boolean block = false;

    private LinkedList<DataRequest> requests;

    private List<TestTO> testsMem;
    private List<Letter> letters;
    private List<Test> tests;
    private List<Problem> problems;

    private DataManager(){
        database = AppDatabase.getInstance(ApplicationController.getContext());
        letterDao = database.getLetterDao();
        testDao =  database.getTestDao();
        wordDao = database.getWordDao();
        letterXWordDao = database.getLetterXWordDao();
        wordXTestDao = database.getWordXTestDao();
        problemDAO = database.getProblemDao();
        childrenDao = database.getChildrenDao();
        testDoneDAO = database.getTestDoneDao();
        resultDao = database.getResultDao();
        requests = new LinkedList<>();
        if(!Preference.isDatabaseFilled()){
            DataRequest<Void> dataRequest = new DataRequest<Void>(null) {
                @Override
                public Void task() {
                    JsonManager jsonManager = new JsonManager();
                    tests = jsonManager.getTests();
                    letters = jsonManager.getLetters();
                    problems = jsonManager.getProblems();
                    fillDatabase();
                    Preference.databaseHasBeenFilled();
                    return null;
                }

                @Override
                public void onTaskEnd() {
                    removeRequest();
                }
            };
            enqueueRequest(dataRequest);
        }
    }


    private void enqueueRequest(DataRequest dataRequest){
        requests.add(dataRequest);
        if (requests.size() == 1 && !block){
            dataRequest.run();
        }
    }

    private void removeRequest(){
        requests.pop();
        if(requests.size() > 0 && !block){
            requests.getFirst().run();
        }
    }

    public static DataManager getInstance(){
        if (instance == null)instance = new DataManager();
        return instance;
    }

    public void block(){
        block = true;
    }

    public void sblock(){
        block = false;
    }

    public void insertTestDone(DataRequestCallback<Long> dataRequestCallback,TestDone testDone){
         DataRequest<Long> dataRequest = new DataRequest<Long>(dataRequestCallback) {
             @Override
             public Long task() {
                 return  testDoneDAO.insertTestDone(testDone);
             }

             @Override
             public void onTaskEnd() {
                 removeRequest();
             }
         };
         enqueueRequest(dataRequest);
    }

    public void getLetters(DataRequestCallback<List<Letter>> dataRequestCallback){
        DataRequest dataRequest = new DataRequest<List<Letter>>(dataRequestCallback){
            @Override
            public List<Letter> task() {
                return letterDao.getAllLetter();
            }

            @Override
            public void onTaskEnd() {
                removeRequest();
            }
        };
        enqueueRequest(dataRequest);
    }

    public void getProblems(DataRequestCallback<List<Problem>> dataRequestCallback){
        DataRequest dataRequest = new DataRequest<List<Problem>>(dataRequestCallback){
            @Override
            public List<Problem> task() {
                try{
                    problemDAO.findAllProblem();
                }catch (Exception e){
                    System.out.print("");
                }
                return problemDAO.findAllProblem();
            }

            @Override
            public void onTaskEnd() {
                removeRequest();
            }
        };
        enqueueRequest(dataRequest);
    }

    public void getTests(DataRequestCallback<List<TestTO>> dataRequestCallback){
        DataRequest dataRequest = new DataRequest<List<TestTO>>(dataRequestCallback){
            @Override
            public List<TestTO> task() {
                /*if (testsMem == null){*/
                    List<TestTO> tests = testDao.findAllTest();
                    for (TestTO t:tests) {
                        List<WordTO> words = wordDao.findWordForTest(t.getId());
                        for (WordTO w:words) {
                            w.setLetters(letterDao.findAllLetterForWord(w.getId()));
                        }
                        t.setWords(words);
                    }
                    testsMem = tests;
                /*}*/
                return testsMem;
            }

            @Override
            public void onTaskEnd() {
                removeRequest();
            }
        };
        enqueueRequest(dataRequest);
    }

    public void getTest(DataRequestCallback<TestTO> dataRequestCallback,int id){
        DataRequest dataRequest = new DataRequest<TestTO>(dataRequestCallback){
            @Override
            public TestTO task() {
                return testDao.findTest(id);
            }

            @Override
            public void onTaskEnd() {
                removeRequest();
            }
        };
        enqueueRequest(dataRequest);
    }

    public void getTestsDone(DataRequestCallback<List<TestDone>> testDoneDataRequestCallback){
        DataRequest<List<TestDone>> doneDataRequest = new DataRequest<List<TestDone>>(testDoneDataRequestCallback) {
            @Override
            public List<TestDone> task() {
                return testDoneDAO.getTestsDone();
            }

            @Override
            public void onTaskEnd() {
                removeRequest();
            }
        };
        enqueueRequest(doneDataRequest);
    }

    public void getChildren(DataRequestCallback<Children> dataRequestCallback,int id){
        DataRequest<Children> dataRequest = new DataRequest<Children>(dataRequestCallback) {
            @Override
            public Children task() {
                return childrenDao.findChildren(id);
            }

            @Override
            public void onTaskEnd() {
                removeRequest();
            }
        };
        enqueueRequest(dataRequest);
    }

    public void getChildrens(DataRequestCallback<List<Children>> dataRequestCallback){
        DataRequest<List<Children>> dataRequest = new DataRequest<List<Children>>(dataRequestCallback) {
            @Override
            public List<Children> task() {
                return childrenDao.findAllChildren();
            }

            @Override
            public void onTaskEnd() {
                removeRequest();
            }
        };
        enqueueRequest(dataRequest);
    }


    public void insertResult(DataRequestCallback<Void> dataRequestCallback, List<ResultTO> results){
        DataRequest<Void> dataRequest = new DataRequest<Void>(dataRequestCallback) {
            @Override
            public Void task() {
                List<Result> resultList = new LinkedList<>();
                for (ResultTO res:results) {
                    Result result = new Result();
                    result.setLetterID(res.getLetterID());
                    result.setIndex(res.getIndex());
                    result.setProblemID(res.getProblemID());
                    result.setReplaceLetterID(res.getReplaceLetterID());
                    result.setTestDoneID(res.getTestDoneID());
                    result.setTestID(res.getTestID());
                    result.setWordID(res.getWordID());
                    resultList.add(result);
                }
                resultDao.insertAll(resultList);
                return null;
            }

            @Override
            public void onTaskEnd() {
                removeRequest();
            }
        };
        enqueueRequest(dataRequest);
    }

    public void getTestDone(DataRequestCallback<TestDone> requestCallback,int id){
        DataRequest<TestDone> request = new DataRequest<TestDone>(requestCallback) {
            @Override
            public TestDone task() {
                return testDoneDAO.getTestDone(id);
            }

            @Override
            public void onTaskEnd() {
                removeRequest();
            }
        };
        enqueueRequest(request);
    }

    public void getResultForTestDone(DataRequestCallback<List<Result>> requestCallback,int id){
        DataRequest<List<Result>> request = new DataRequest<List<Result>>(requestCallback) {
            @Override
            public List<Result> task() {
                return resultDao.getResult(id);
            }

            @Override
            public void onTaskEnd() {
                removeRequest();
            }
        };
        enqueueRequest(request);
    }

    public void getAllWords(DataRequestCallback<List<Word>> dataRequestCallback){
        DataRequest<List<Word>> dataRequest = new DataRequest<List<Word>>(dataRequestCallback) {
            @Override
            public List<Word> task() {
                return wordDao.getAllWords();
            }

            @Override
            public void onTaskEnd() {
                removeRequest();
            }
        };
        enqueueRequest(dataRequest);
    }


    public void insertTest(TestTO testTO){
        Test t = new Test();
        t.setDescription(testTO.getDescription());
        t.setTitle(testTO.getTitle());
        t.setLevel(testTO.getLevel());
        long newTestID = testDao.insertTest(t);

        for (int i = 0;i < testTO.getWords().size();i++) {

            WordTO w = testTO.getWords().get(i);
            Word newWord = new Word();
            newWord.setWordText(w.getWordText());

            long newWordID = wordDao.insertWord(newWord);

            WordXTest wordXTest = new WordXTest();
            wordXTest.setTestID((int)newTestID);
            wordXTest.setWordID((int)newWordID);
            wordXTest.setIndex(i);
            wordXTestDao.insertWordXTest(wordXTest);

            for (int j = 0;j < w.getLetters().size();j++) {
                LetterTO l = w.getLetters().get(j);
                Letter newLetter = new Letter();
                newLetter.setText(l.getText());
                newLetter.setVocal(l.isVocal());

                long newLetterID = letterDao.insertLetter(newLetter);

                LetterXWord letterXWord = new LetterXWord();
                letterXWord.setIndex(j);
                letterXWord.setLetterID((int)newLetterID);
                letterXWord.setWordID((int)newWordID);
                letterXWordDao.insertLetterXWord(letterXWord);
            }
        }
    }

    public void fillDatabase(){
        this.insertLetterV2();
        this.insertWordV2();
        this.insertTest();
        this.insertProblemV2();
        this.insertChildren();
    }

    private void insertChildren(){
        Children children = new Children();
        children.setName("Graziano");
        children.setSurname("Rizzi");
        children.setBornDate(new Date().toString());
        children.setCode("RZZGZN91A22H096E");
        children.setMale(true);
        children.setEmail("graziano.rizzi1991@gmail.com");
        childrenDao.insertChildren(children);
    }

    public void insertNewChildren(DataRequestCallback<Void> dataRequestCallback,Children children){
        DataRequest<Void> dataRequest = new DataRequest<Void>(dataRequestCallback) {
            @Override
            public Void task() {
                childrenDao.insertChildren(children);
                return null;
            }

            @Override
            public void onTaskEnd() {
                removeRequest();
            }
        };
        enqueueRequest(dataRequest);
    }

    public void removeChildren(DataRequestCallback<Void> dataRequestCallback,int id){
        DataRequest<Void> dataRequest = new DataRequest<Void>(dataRequestCallback) {
            @Override
            public Void task() {
                childrenDao.deleteChildren(id);
                return null;
            }

            @Override
            public void onTaskEnd() {
                removeRequest();
            }
        };
        enqueueRequest(dataRequest);
    }

    public void insertProblem(){
        LinkedList<Problem> problems = new LinkedList<>();
        Problem p = new Problem();
        p.setDesc("Sostituzione lettera");
        p.setDialog(false);
        problems.add(p);
        final String[] items = {
                "Nasalità non appropriata", "Impreciso punto di articolazione", "Splashing","Produzione interdentale",
                "Ridotta mobilità labiale","Vibrazione non adeguata","Omessa","Altro"
        };
        for(int i = 0;i < items.length;i++){
            Problem pn = new Problem();
            pn.setDesc(items[i]);
            pn.setDialog(true);
            problems.add(pn);
        }
        problemDAO.insertProblems(problems);
    }

    public void insertProblemV2(){
        problemDAO.insertProblems(problems);
    }

    public void insertTest(){
        for (Test t:this.tests) {
            long testID = testDao.insertTest(t);
            int j = 0;
            for (Word w:t.getWords()) {
                long wordID = wordDao.getWordIDForText(w.getWordText());
                if(wordID == -1){
                    System.out.println("PROBLEMA");
                }
                WordXTest wordXTest = new WordXTest();
                wordXTest.setIndex(j);//qui bisognerebbe mettere un +1
                wordXTest.setWordID((int)wordID);
                wordXTest.setTestID((int)testID);
                wordXTestDao.insertWordXTest(wordXTest);
                j++;
            }
        }
    }

    private class testInsHelper{
        public Test test;
        String[] words;
    }

    public void insertWord(){
        String wordsString = "g a l: o\n" +
                "t ɔ p o\n" +
                "k u b i\n" +
                "p i t:s a\n" +
                "s u g o \n" +
                "p j Ɛ d e \n" +
                "tʃ u t:ʃ o\n" +
                "d o t:ʃ a\n" +
                "l Ɛ t: o \n" +
                "s Ɛ d j a \n" +
                "tʃ Ɛ l o \n" +
                "f u m o\n" +
                "n e v e\n" +
                "s a s: o\n" +
                "p e ʃ: e\n" +
                "m a m: a\n" +
                "v o l a\n" +
                "b u s: a\n" +
                "e ʃ: e\n" +
                "s o f: j a\n" +
                "s a b: j a\n" +
                "a p e\n" +
                "g a t: o\n" +
                "k a n e\n" +
                "b o k: a\n" +
                "m a g o \n" +
                "d i t o\n" +
                "f a t a\n" +
                "k a f: Ɛ\n" +
                "v a z o\n" +
                "u v a\n" +
                "t a t: s a\n" +
                "m u k: a\n" +
                "l u n a\n" +
                "n a z o";
        String[] words = wordsString.split("\n");
        String[] images = {"GALLO","TOPO","CUBI","PIZZA","SUGO","PIEDE","CIUCCIO","DOCCIA","LETTO",
                "SEDIA","CIELO","FUMO","NEVE","SASSO","PESCE","MAMMA","VOLA","BUSSA",
        "ESCE1","SOFFIA","SABBIA","APE","GATTO","CANE","BOCCA","MAGO","DITO","FATA","CAFFE","VASO",
        "UVA","TAZZA","MUCCA","LUNA","NASO"};

        for(int i = 0; i < words.length;i++){
            Word word = new Word();
            word.setWordText(words[i].replace(" ",""));
            word.setImage_name(images[i].toLowerCase());
            long wordID = wordDao.insertWord(word);
            if(wordID == -1){
                wordID = wordDao.getWordIDForText(word.getWordText());
            }
            String[] chars = words[i].split(" ");
            for(int j = 0; j < chars.length;j++){
                long letterID = letterDao.getLetterID(chars[j]);
                if(letterID == 0){
                    System.out.println();
                }
                LetterXWord letterXWord = new LetterXWord();
                letterXWord.setWordID((int)wordID);
                letterXWord.setLetterID((int)letterID);
                letterXWord.setIndex(j);
                letterXWordDao.insertLetterXWord(letterXWord);
            }
        }
    }

    public void insertWordV2(){
        HashSet<Word> wordHashSet = new HashSet<>();
        for (Test t:tests) {
            for (Word w:t.getWords()) {
                wordHashSet.add(w);
            }
        }
        Word[] words = wordHashSet.toArray(new Word[0]);
        for(int i = 0; i < words.length;i++){
            Word word = new Word();
            word.setWordText(words[i].getWordText().replace(" ",""));
            word.setImage_name(words[i].getWordText().toLowerCase());
            long wordID = wordDao.insertWord(word);
            if(wordID == -1){
                wordID = wordDao.getWordIDForText(word.getWordText());
            }
            String[] chars = words[i].getLetters().toArray(new String[0]);
            for(int j = 0; j < chars.length;j++){
                long letterID = letterDao.getLetterID(chars[j]);
                if(letterID == 0){
                    System.out.println();
                }
                LetterXWord letterXWord = new LetterXWord();
                letterXWord.setWordID((int)wordID);
                letterXWord.setLetterID((int)letterID);
                letterXWord.setIndex(j);
                letterXWordDao.insertLetterXWord(letterXWord);
            }
        }
    }

    public void insertLetter(){
        String wordsString = "g a l: o " +
                "t ɔ p o " +
                "k u b i " +
                "p i t:s a " +
                "s u g o  " +
                "p j Ɛ d e  " +
                "tʃ u t:ʃ o " +
                "d o t:ʃ a " +
                "l Ɛ t: o  " +
                "s Ɛ d j a  " +
                "tʃ Ɛ l o  " +
                "f u m o " +
                "n e v e " +
                "s a s: o " +
                "p e ʃ: e " +
                "m a m: a " +
                "v o l a " +
                "b u s: a " +
                "e ʃ: e " +
                "s o f: j a " +
                "s a b: j a " +
                "a p e " +
                "g a t: o " +
                "k a n e " +
                "b o k: a " +
                "m a g o  " +
                "d i t o " +
                "f a t a " +
                "k a f: Ɛ " +
                "v a z o " +
                "u v a " +
                "t a t: s a " +
                "m u k: a " +
                "l u n a " +
                "n a z o";
        String[] words = wordsString.split(" ");

        String lettersString = "a\n" +
                "b\n" +
                "tʃ\n" +
                "k\n" +
                "d\n" +
                "e\n" +
                "Ɛ\n" +
                "f\n" +
                "g\n" +
                "dʒ\n" +
                "ʎ\n" +
                "i\n" +
                "j\n" +
                "l\n" +
                "m\n" +
                "n\n" +
                "ɲ\n" +
                "ŋ\n" +
                "o\n" +
                "ɔ\n" +
                "p\n" +
                "r\n" +
                "s\n" +
                "z\n" +
                "ʃ\n" +
                "t\n" +
                "u\n" +
                "w\n" +
                "v\n" +
                "dz\n" +
                "ts\n";

        String[] letters = lettersString.split("\n");
        String[] sillab = words;

        LinkedHashSet<Letter> lettersSet = new LinkedHashSet<>();

        for(int i = 0;i < letters.length;i++){
            //char actualChar = (char)((int)'a' + i);
            Letter newLetter = new Letter();
            newLetter.setAvKeyboard(true);
            newLetter.setText(letters[i]);
            try {
                lettersSet.add(newLetter);
            }catch (Exception e){
                e.printStackTrace();
            }

        }

        for (int i = 0;i < sillab.length;i++){
            Letter newLetter = new Letter();
            newLetter.setAvKeyboard(false);
            newLetter.setText(sillab[i]);
            try {
                lettersSet.add(newLetter);
            }catch (Exception e){
                e.printStackTrace();
            }
        }

        for (Letter l:lettersSet) {
            letterDao.insertLetter(l);
        }
    }

    public void insertLetterV2(){
        for (Letter l:letters) {
            letterDao.insertLetter(l);
        }
    }

    public void getLettersID(){
        LinkedList<Long> ids = new LinkedList<>();
        for(int i = 0;i < 25;i++){
            char actualChar = (char)((int)'a' + i);
            Long id = letterDao.getLetterID("" + actualChar);
            ids.add(id);
        }
    }

    public abstract class DataRequest<T> {

        private DataRequestCallback<T> dataRequestCallback;

        DataRequest(DataRequestCallback<T> dataRequestCallback){
            this.dataRequestCallback = dataRequestCallback;
        }

        public final void run(){
            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    try{
                        T t = task();
                        onTaskEnd();
                        if(dataRequestCallback!=null)dataRequestCallback.onSuccess(t);
                    }catch (Exception e){
                        if(dataRequestCallback!=null)dataRequestCallback.onFail(e);
                    }
                }
            });
            thread.start();
        }


        public abstract T task();
        public abstract void onTaskEnd();
    }

}
