package rizzi.graziano.inventariofonetico.data;


public abstract class DataRequest<T> {

    private DataRequestCallback<T> dataRequestCallback;

    DataRequest(DataRequestCallback<T> dataRequestCallback){
        this.dataRequestCallback = dataRequestCallback;
    }

    public final void run(){
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try{
                    T t = task();
                    onTaskEnd();
                    if(dataRequestCallback!=null)dataRequestCallback.onSuccess(t);
                }catch (Exception e){
                    if(dataRequestCallback!=null)dataRequestCallback.onFail(e);
                }
            }
        });
        thread.start();
    }


    public abstract T task();
    public abstract void onTaskEnd();
}
