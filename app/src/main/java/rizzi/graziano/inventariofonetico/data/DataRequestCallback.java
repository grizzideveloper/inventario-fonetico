package rizzi.graziano.inventariofonetico.data;

public interface DataRequestCallback<T> {
    void onSuccess(T dataResult);
    void onFail(Exception e);
}
