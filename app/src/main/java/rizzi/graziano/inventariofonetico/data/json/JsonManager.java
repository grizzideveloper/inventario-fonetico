package rizzi.graziano.inventariofonetico.data.json;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;

import rizzi.graziano.inventariofonetico.data.persistence.entity.Letter;
import rizzi.graziano.inventariofonetico.data.persistence.entity.Problem;
import rizzi.graziano.inventariofonetico.data.persistence.entity.Test;
import rizzi.graziano.inventariofonetico.data.persistence.entity.Word;
import rizzi.graziano.inventariofonetico.data.to.WordTO;

import static rizzi.graziano.inventariofonetico.controller.ApplicationController.getContext;

public class JsonManager {

    private List<Test> tests = new LinkedList<>();
    private List<Letter> letters = new LinkedList<>();
    private List<Problem> problems = new LinkedList<>();
    private List<Word> words = new LinkedList<>();

    public JsonManager() {
        try {
            JSONObject obj = new JSONObject(loadJSONFromAsset());
            JSONArray lettere = obj.getJSONArray("lettere");



            for(int i = 0;i < lettere.length();i++){
                Letter letter = new Letter();
                JSONObject lettera = lettere.getJSONObject(i);
                String testoLettera = lettera.getString("lettera");
                int abilitazioneTastiera = lettera.getInt("abilitato_alla_tastiera");
                int vocale = lettera.getInt("vocale");
                letter.setText(testoLettera);
                letter.setAvKeyboard(abilitazioneTastiera == 1);
                letter.setVocal(vocale == 1);
                letters.add(letter);
            }


            HashSet<Word> wordHashSet = new HashSet<>();
            JSONArray testsJson = obj.getJSONArray("Test");
            for (int i = 0; i < testsJson.length();i++){
                JSONObject testJson = testsJson.getJSONObject(i);
                JSONArray parole = testJson.getJSONArray("parole");
                Test test = new Test();
                List<Word> words = new LinkedList<>();
                for (int j = 0; j < parole.length();j++){
                    JSONObject parola = parole.getJSONObject(j);
                    JSONArray lettereDellaParola = parola.getJSONArray("lettere");
                    List<String> lettersForWord = new LinkedList<>();
                    for (int k = 0;k < lettereDellaParola.length();k++){
                        String letteraDellaParola = lettereDellaParola.getString(k);
                        lettersForWord.add(letteraDellaParola);
                    }
                    Word word = new Word();
                    word.setImage_name(parola.getString("parola"));
                    word.setWordText(parola.getString("parola"));
                    word.setLetters(lettersForWord);
                    words.add(word);
                    wordHashSet.add(word);
                }
                test.setDescription(testJson.getString("descrizione"));
                test.setLevel(testJson.getInt("livello"));
                test.setTitle(testJson.getString("titolo"));
                test.setWords(words);
                tests.add(test);
            }

            words.addAll(Arrays.asList(wordHashSet.toArray(new Word[0])));

            JSONArray jsonProblemArray = obj.getJSONArray("problemi");
            for (int i = 0; i < jsonProblemArray.length();i++){
                JSONObject problemJson = jsonProblemArray.getJSONObject(i);
                Problem problem = new Problem();
                problem.setDialog(problemJson.getString("dialog").equals("1"));
                problem.setDesc(problemJson.getString("descrizione"));
                problems.add(problem);
            }


            JSONObject nJson = new JSONObject();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public JSONObject problemJsonObj(){
        return new JSONObject();
    }

    public JSONObject testJsonObj(){
        return new JSONObject();
    }

    public JSONObject wordJsonObj(){
        return new JSONObject();
    }

    public JSONObject letterJsonObj(){
        return new JSONObject();
    }

    public List<Test> getTests() {
        return tests;
    }

    public List<Letter> getLetters() {
        return letters;
    }

    public List<Problem> getProblems(){return problems;}

    @Nullable
    public String loadJSONFromAsset() {
        String json = null;
        try {
            InputStream is = getContext().getAssets().open("json_inv_fon.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }
}

