package rizzi.graziano.inventariofonetico.data.persistence;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import java.util.LinkedList;
import java.util.List;

import rizzi.graziano.inventariofonetico.data.persistence.dao.ChildrenDAO;
import rizzi.graziano.inventariofonetico.data.persistence.dao.LetterDao;
import rizzi.graziano.inventariofonetico.data.persistence.dao.LetterXWordDao;
import rizzi.graziano.inventariofonetico.data.persistence.dao.ProblemDAO;
import rizzi.graziano.inventariofonetico.data.persistence.dao.ResultDao;
import rizzi.graziano.inventariofonetico.data.persistence.dao.TestDao;
import rizzi.graziano.inventariofonetico.data.persistence.dao.TestDoneDAO;
import rizzi.graziano.inventariofonetico.data.persistence.dao.WordDao;
import rizzi.graziano.inventariofonetico.data.persistence.dao.WordXTestDao;
import rizzi.graziano.inventariofonetico.data.persistence.entity.Children;
import rizzi.graziano.inventariofonetico.data.persistence.entity.Letter;
import rizzi.graziano.inventariofonetico.data.persistence.entity.LetterXWord;
import rizzi.graziano.inventariofonetico.data.persistence.entity.Problem;
import rizzi.graziano.inventariofonetico.data.persistence.entity.Result;
import rizzi.graziano.inventariofonetico.data.persistence.entity.Test;
import rizzi.graziano.inventariofonetico.data.persistence.entity.TestDone;
import rizzi.graziano.inventariofonetico.data.persistence.entity.Word;
import rizzi.graziano.inventariofonetico.data.persistence.entity.WordXTest;
import rizzi.graziano.inventariofonetico.data.to.TestTO;
import rizzi.graziano.inventariofonetico.data.to.WordTO;

@Database(entities = {Children.class, Letter.class, LetterXWord.class,
        Result.class, Test.class, TestDone.class, Word.class, WordXTest.class, Problem.class},version = 2)
public abstract class AppDatabase extends RoomDatabase{

    public abstract WordDao getWordDao();
    public abstract LetterDao getLetterDao();
    public abstract TestDao getTestDao();
    public abstract WordXTestDao getWordXTestDao();
    public abstract LetterXWordDao getLetterXWordDao();
    public abstract ProblemDAO getProblemDao();
    public abstract ChildrenDAO getChildrenDao();
    public abstract TestDoneDAO getTestDoneDao();
    public abstract ResultDao getResultDao();

    private static TestDao testDao;
    private static WordDao wordDao;
    private static LetterDao letterDao;
    private static WordXTestDao wordXTestDao;
    private static LetterXWordDao letterXWordDao;
    private static ProblemDAO problemDAO;
    private static ChildrenDAO childrenDAO;
    private static TestDoneDAO testDoneDAO;
    private static boolean databaseReady = false;


    private static AppDatabase INSTANCE;


    public synchronized static AppDatabase getInstance(Context context) {

        if (INSTANCE == null) {
            INSTANCE = buildDatabase(context);
        }
        return INSTANCE;
    }

    private static AppDatabase buildDatabase(final Context context) {
        return Room.databaseBuilder(context,
                AppDatabase.class,
                "my-database")
                .build();
    }


    public List<TestTO> getTests(){
        List<TestTO> tests = testDao.findAllTest();
        for (TestTO t:tests) {
            List<WordTO> words = wordDao.findWordForTest(t.getId());
            for (WordTO w:words) {
                w.setLetters(letterDao.findAllLetterForWord(w.getId()));
            }
            t.setWords(words);
        }
        return tests;
    }
}
