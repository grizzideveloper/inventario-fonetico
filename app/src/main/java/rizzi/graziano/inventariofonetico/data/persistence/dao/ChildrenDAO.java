package rizzi.graziano.inventariofonetico.data.persistence.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

import rizzi.graziano.inventariofonetico.data.persistence.entity.Children;

@Dao
public interface ChildrenDAO {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    long insertChildren(Children children);

    @Query("SELECT * FROM children")
    List<Children> findAllChildren();

    @Query("SELECT * FROM children WHERE uid = :id")
    Children findChildren(int id);

    @Query("DELETE FROM children WHERE uid= :id")
    void deleteChildren(int id);
}
