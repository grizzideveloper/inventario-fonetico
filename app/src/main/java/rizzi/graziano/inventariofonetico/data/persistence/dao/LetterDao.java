package rizzi.graziano.inventariofonetico.data.persistence.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

import rizzi.graziano.inventariofonetico.data.persistence.entity.Letter;
import rizzi.graziano.inventariofonetico.data.to.LetterTO;

@Dao
public interface LetterDao {

    @Query("SELECT letter.uid as uid,letter.text as text,letter.isVocal as isVocal,letterxword.`index` FROM letter JOIN letterxword ON letter.uid = letterxword.letterID WHERE letterxword.wordID = :wordID")
    List<LetterTO> findAllLetterForWord(int wordID);

    @Query("SELECT * FROM letter ORDER BY uid")
    List<Letter> getAllLetter();

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    long insertLetter(Letter l);

    @Query("SELECT letter.uid FROM letter WHERE letter.text = :text")
    long getLetterID(String text);
}
