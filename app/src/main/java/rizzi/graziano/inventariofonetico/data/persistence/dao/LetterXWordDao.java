package rizzi.graziano.inventariofonetico.data.persistence.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;

import rizzi.graziano.inventariofonetico.data.persistence.entity.LetterXWord;

@Dao
public interface LetterXWordDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    long insertLetterXWord(LetterXWord letterXWord);
}
