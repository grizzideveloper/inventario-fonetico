package rizzi.graziano.inventariofonetico.data.persistence.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

import rizzi.graziano.inventariofonetico.data.persistence.entity.Problem;

@Dao
public interface ProblemDAO {

    @Insert
    long insertProblem(Problem problem);

    @Insert
    void insertProblems(List<Problem> problems);

    @Query("SELECT * FROM problem")
    List<Problem> findAllProblem();
}
