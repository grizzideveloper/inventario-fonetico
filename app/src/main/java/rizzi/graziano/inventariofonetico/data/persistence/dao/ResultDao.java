package rizzi.graziano.inventariofonetico.data.persistence.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

import rizzi.graziano.inventariofonetico.data.persistence.entity.Result;
import rizzi.graziano.inventariofonetico.data.to.ResultTO;
@Dao
public interface ResultDao {

    @Insert
    long insert(Result result);

    @Insert
    long[] insertAll(List<Result> resultList);

    @Query("SELECT * FROM result WHERE testDoneID = :testDoneId")
    List<Result> getResult(int testDoneId);
}
