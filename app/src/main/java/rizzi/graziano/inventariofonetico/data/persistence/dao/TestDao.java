package rizzi.graziano.inventariofonetico.data.persistence.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

import rizzi.graziano.inventariofonetico.data.persistence.entity.Test;
import rizzi.graziano.inventariofonetico.data.to.TestTO;

@Dao
public interface TestDao {

    @Query("SELECT * FROM test")
    List<TestTO> findAllTest();

    @Query("SELECT * FROM test WHERE uid = :id")
    TestTO findTest(int id);

    @Insert
    long insertTest(Test t);
}
