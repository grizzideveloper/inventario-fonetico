package rizzi.graziano.inventariofonetico.data.persistence.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

import rizzi.graziano.inventariofonetico.data.persistence.entity.TestDone;

@Dao
public interface TestDoneDAO {
    @Insert
    long insertTestDone(TestDone testDone);

    @Query("SELECT * FROM testdone WHERE uid = :id")
    TestDone getTestDone(int id);

    @Query("SELECT * FROM testdone")
    List<TestDone> getTestsDone();
}
