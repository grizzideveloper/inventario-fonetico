package rizzi.graziano.inventariofonetico.data.persistence.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

import rizzi.graziano.inventariofonetico.data.persistence.entity.Word;
import rizzi.graziano.inventariofonetico.data.to.WordTO;

@Dao
public interface  WordDao {

    @Query("SELECT word.uid as uid, word.wordText as wordText,word.image_name as image_name ,wordxtest.`index` FROM word JOIN wordxtest ON word.uid = wordxtest.wordID WHERE wordxtest.testID = :testID")
    List<WordTO> findWordForTest(int testID);

    @Query("SELECT * FROM word")
    List<Word> getAllWords();

    @Query("SELECT uid FROM word WHERE word.wordText = :text")
    long getWordIDForText(String text);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    long insertWord(Word w);
}
