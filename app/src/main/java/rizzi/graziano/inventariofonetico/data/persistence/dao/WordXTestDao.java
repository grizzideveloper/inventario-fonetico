package rizzi.graziano.inventariofonetico.data.persistence.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;

import rizzi.graziano.inventariofonetico.data.persistence.entity.WordXTest;

@Dao
public interface WordXTestDao {

    @Insert
    long insertWordXTest(WordXTest wordXTest);
}
