package rizzi.graziano.inventariofonetico.data.persistence.entity;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;

@Entity(indices = @Index(value = "text",unique = true))
public class Letter extends BaseEntity {

    private String text;
    private boolean isVocal;
    private boolean avKeyboard;

    @PrimaryKey(autoGenerate = true)
    private int uid;

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public Letter(){
        text = "";
        isVocal = false;
    }

    public String getText() {
        return text;
    }

    public String getLetter(){
        return text.substring(0,1);
    }

    public void setText(String text) {
        //questo mi rimuove gli spazzi che non devono esserci in una lettera
        this.text = text.replace(" ","");
    }

    public boolean isVocal() {
        return isVocal;
    }

    public void setVocal(boolean vocal) {
        isVocal = vocal;
    }

    public boolean isAvKeyboard() {
        return avKeyboard;
    }

    public void setAvKeyboard(boolean avKeyboard) {
        this.avKeyboard = avKeyboard;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof Letter){
            if(obj == this || this.getText() == ((Letter) obj).getText())return true;
        }
        return false;
    }

    @Override
    public String toString() {
        return text + "\n";
    }
}
