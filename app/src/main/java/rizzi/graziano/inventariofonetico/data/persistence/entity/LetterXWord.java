package rizzi.graziano.inventariofonetico.data.persistence.entity;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;

@Entity(foreignKeys = {
        @ForeignKey(entity = Letter.class,parentColumns = "uid",childColumns = "letterID"),
        @ForeignKey(entity = Word.class,parentColumns = "uid",childColumns = "wordID")},
        indices = {
                @Index(value = {"wordID","index"}, unique = true),
                @Index(value = "letterID")//Perchè mi viene chiesto come warning XD
        })
public class LetterXWord {

    private int letterID;
    private int wordID;
    private int index;

    @PrimaryKey(autoGenerate = true)
    private int uid;

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public int getLetterID() {
        return letterID;
    }

    public void setLetterID(int letterID) {
        this.letterID = letterID;
    }

    public int getWordID() {
        return wordID;
    }

    public void setWordID(int wordID) {
        this.wordID = wordID;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }
}
