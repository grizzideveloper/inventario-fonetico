package rizzi.graziano.inventariofonetico.data.persistence.entity;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;

@Entity(foreignKeys = {
        @ForeignKey(entity = LetterXWord.class,parentColumns = {"wordID","index"},childColumns = {"wordID","index"}),
        @ForeignKey(entity = WordXTest.class,parentColumns = {"wordID","testID"},childColumns = {"wordID","testID"}),
        @ForeignKey(entity = TestDone.class,parentColumns = {"testID","uid"},childColumns = {"testID","testDoneID"}),
        @ForeignKey(entity = Problem.class,parentColumns = {"uid"},childColumns = {"problemID"}),
        @ForeignKey(entity = Letter.class,parentColumns = {"uid"},childColumns = {"replaceLetterID"})},
        indices = {
                @Index(value = {"wordID","letterID","index","testID","testDoneID"},unique = true),
                @Index(value = {"wordID","testID"},unique = false),//Queste due sono per un warning
                @Index(value = {"testID","testDoneID"},unique = false)
        }
)
public class Result extends BaseEntity {

    private int testDoneID;
    private int testID;
    private int wordID;
    private int letterID;
    private int index;
    private int problemID;
    private int replaceLetterID;

    @PrimaryKey(autoGenerate = true)
    private int uid;

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public int getTestDoneID() {
        return testDoneID;
    }

    public void setTestDoneID(int testDoneID) {
        this.testDoneID = testDoneID;
    }

    public int getTestID() {
        return testID;
    }

    public void setTestID(int testID) {
        this.testID = testID;
    }

    public int getWordID() {
        return wordID;
    }

    public void setWordID(int wordID) {
        this.wordID = wordID;
    }

    public int getLetterID() {
        return letterID;
    }

    public void setLetterID(int letterID) {
        this.letterID = letterID;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public int getProblemID() {
        return problemID;
    }

    public void setProblemID(int problemID) {
        this.problemID = problemID;
    }

    public int getReplaceLetterID() {
        return replaceLetterID;
    }

    public void setReplaceLetterID(int replaceLetterID) {
        this.replaceLetterID = replaceLetterID;
    }
}
