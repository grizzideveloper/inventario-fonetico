package rizzi.graziano.inventariofonetico.data.persistence.entity;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;

@Entity(foreignKeys = {
        @ForeignKey(entity = Test.class,parentColumns = "uid",childColumns = "testID"),
        @ForeignKey(entity = Children.class,parentColumns = "uid",childColumns = "childrenID")},
        indices = {
                @Index(value = {"testID","childrenID"},unique = false),
                @Index(value = {"uid","testID"},unique = true)//inutile dato che uid è chiave primaria ma la libreria rompe le palle
        })
public class TestDone extends BaseEntity {

    private int testID;
    private int childrenID;
    private String date;

    @PrimaryKey(autoGenerate = true)
    private int uid;

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public int getTestID() {
        return testID;
    }

    public void setTestID(int testID) {
        this.testID = testID;
    }

    public int getChildrenID() {
        return childrenID;
    }

    public void setChildrenID(int childrenID) {
        this.childrenID = childrenID;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
