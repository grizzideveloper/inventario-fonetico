package rizzi.graziano.inventariofonetico.data.persistence.entity;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;

import java.util.LinkedList;
import java.util.List;

@Entity(indices = @Index(value = "wordText",unique = true))
public class Word extends BaseEntity {

    private String wordText;
    private String image_name;

    @Ignore
    private List<String> letters = new LinkedList<>();

    @PrimaryKey(autoGenerate = true)
    private int uid;

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public String getWordText() {
        return wordText;
    }

    public void setWordText(String wordText) {
        this.wordText = wordText;
    }

    public String getImage_name() {
        return image_name;
    }

    public void setImage_name(String image_name) {
        this.image_name = image_name;
    }

    public List<String> getLetters() {
        return letters;
    }

    public void setLetters(List<String> letters) {
        this.letters = letters;
    }

    @Override
    public boolean equals(Object obj) {
        return (obj instanceof Word) && ((Word) obj).wordText.equals(this.wordText);
    }

    @Override
    public int hashCode() {
        return 0;
    }
}
