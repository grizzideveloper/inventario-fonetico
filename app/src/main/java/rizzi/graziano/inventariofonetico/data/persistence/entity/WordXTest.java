package rizzi.graziano.inventariofonetico.data.persistence.entity;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;

@Entity(foreignKeys = {
        @ForeignKey(entity = Word.class,parentColumns = "uid",childColumns = "wordID"),
        @ForeignKey(entity = Test.class,parentColumns = "uid",childColumns = "testID")},
        indices = {
                @Index(value = {"testID","wordID"},unique = true),
                @Index(value = {"testID","index"},unique = true)
        }
)

public class WordXTest extends BaseEntity {

    private int wordID;
    private int testID;
    private int index;

    @PrimaryKey(autoGenerate = true)
    private int uid;

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public int getWordID() {
        return wordID;
    }

    public void setWordID(int wordID) {
        this.wordID = wordID;
    }

    public int getTestID() {
        return testID;
    }

    public void setTestID(int testID) {
        this.testID = testID;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }
}
