package rizzi.graziano.inventariofonetico.data.persistence.entity;

import java.util.LinkedList;
import java.util.List;

public class WrongWord {

    private String word;
    private List<String> errors = new LinkedList<>();

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public List<String> getErrors() {
        return errors;
    }

    public void setErrors(List<String> errors) {
        this.errors = errors;
    }
}
