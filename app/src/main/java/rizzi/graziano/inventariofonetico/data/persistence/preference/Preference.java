package rizzi.graziano.inventariofonetico.data.persistence.preference;


import android.content.Context;
import android.content.SharedPreferences;

import rizzi.graziano.inventariofonetico.controller.ApplicationController;

public class Preference {

    private static final String MyPREFERENCES = "mypreference";
    private static final String DATABASEFILLED = "databasefilled";
    private static SharedPreferences sharedPreferences  = ApplicationController.getContext().getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);


    public static boolean isDatabaseFilled(){
        return sharedPreferences.getBoolean(DATABASEFILLED,false);
    }

    public static void databaseHasBeenFilled(){
        sharedPreferences.edit().putBoolean(DATABASEFILLED,true).apply();
    }
}
