package rizzi.graziano.inventariofonetico.data.to;

public class ChildrenTO extends TO {

    private String name;
    private String surname;
    private String bornDate;
    private boolean male;
    private String code;
    private int uid;

    public ChildrenTO(String name, String surname, String bornDate, boolean male, String code, int uid) {
        this.name = name;
        this.surname = surname;
        this.bornDate = bornDate;
        this.male = male;
        this.code = code;
        this.uid = uid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getBornDate() {
        return bornDate;
    }

    public void setBornDate(String bornDate) {
        this.bornDate = bornDate;
    }

    public boolean isMale() {
        return male;
    }

    public void setMale(boolean male) {
        this.male = male;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }
}
