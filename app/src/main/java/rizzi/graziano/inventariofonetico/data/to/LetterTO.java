package rizzi.graziano.inventariofonetico.data.to;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Ignore;

import rizzi.graziano.inventariofonetico.data.persistence.entity.Letter;

public class LetterTO {

    @ColumnInfo(name = "uid")
    private int id;
    @ColumnInfo(name = "text")
    private String text;
    @ColumnInfo(name = "isVocal")
    private boolean isVocal;
    @ColumnInfo(name = "index")
    private int index;


    @Ignore
    private ResultTO result = null;

    public LetterTO(Letter letter,int index){
        this.id = letter.getUid();
        this.text = letter.getText();
        this.isVocal = letter.isVocal();
        this.index = index;
    }

    public LetterTO(){

    }

    public int getId() {
        return id;
    }

    public String getText() {
        return text;
    }

    public boolean isVocal() {
        return isVocal;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void setVocal(boolean vocal) {
        isVocal = vocal;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public ResultTO getResult() {
        return result;
    }

    public void setResult(ResultTO result) {
        this.result = result;
    }

    public void clearResult(){
        result = null;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj != null){
            if(this != obj){
                return obj instanceof LetterTO && ((LetterTO)obj).id == this.id;
            }else return true;
        }else return false;
    }
}
