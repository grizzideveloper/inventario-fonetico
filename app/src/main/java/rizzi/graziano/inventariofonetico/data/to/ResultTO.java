package rizzi.graziano.inventariofonetico.data.to;

import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;

public class ResultTO extends TO {


    private int testDoneID;
    private int testID;
    private int wordID;
    private int letterID;
    private int index;
    private int problemID;
    private int replaceLetterID;

    @Ignore
    private String letterText = "";

    @PrimaryKey(autoGenerate = true)
    private int uid;

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public int getTestDoneID() {
        return testDoneID;
    }

    public void setTestDoneID(int testDoneID) {
        this.testDoneID = testDoneID;
    }

    public int getTestID() {
        return testID;
    }

    public void setTestID(int testID) {
        this.testID = testID;
    }

    public int getWordID() {
        return wordID;
    }

    public void setWordID(int wordID) {
        this.wordID = wordID;
    }

    public int getLetterID() {
        return letterID;
    }

    public void setLetterID(int letterID) {
        this.letterID = letterID;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getLetter() {
        return letterText;
    }

    public void setLetter(String letterText) {
        this.letterText = letterText;
    }

    public int getProblemID() {
        return problemID;
    }

    public void setProblemID(int problemID) {
        this.problemID = problemID;
    }

    public int getReplaceLetterID() {
        return replaceLetterID;
    }

    public void setReplaceLetterID(int replaceLetterID) {
        this.replaceLetterID = replaceLetterID;
    }
}
