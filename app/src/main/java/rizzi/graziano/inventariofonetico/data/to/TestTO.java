package rizzi.graziano.inventariofonetico.data.to;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Ignore;

import java.util.List;

import rizzi.graziano.inventariofonetico.data.persistence.entity.Test;

public class TestTO extends TO{

    @ColumnInfo(name = "uid")
    private int id;
    @ColumnInfo(name = "title")
    private String title;
    @ColumnInfo(name = "description")
    private String description;
    @ColumnInfo(name = "level")
    private int level;

    @Ignore
    private List<WordTO> words;

    public TestTO(Test test,List<WordTO> words){
        this.id = test.getUid();
        this.level = test.getLevel();
        this.title = test.getTitle();
        this.description = test.getDescription();
    }

    public TestTO(){

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public List<WordTO> getWords() {
        return words;
    }

    public void setWords(List<WordTO> words) {
        this.words = words;
    }
}
