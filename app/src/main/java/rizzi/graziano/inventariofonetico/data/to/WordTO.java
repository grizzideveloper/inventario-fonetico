package rizzi.graziano.inventariofonetico.data.to;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Ignore;

import java.util.List;

import rizzi.graziano.inventariofonetico.data.persistence.entity.Word;

public class WordTO {

    @ColumnInfo(name = "uid")
    private int id;

    @ColumnInfo(name = "wordText")
    private String wordText;

    @Ignore
    private List<LetterTO> letters;

    @ColumnInfo(name = "index")
    private int index;

    @ColumnInfo(name = "image_name")
    private String image_name;

    public WordTO(Word word,List<LetterTO> letters){
        this.id = word.getUid();
        this.wordText = word.getWordText();
        this.letters = letters;
    }

    public WordTO(){

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getWordText() {
        return wordText;
    }

    public void setWordText(String wordText) {
        this.wordText = wordText;
    }

    public List<LetterTO> getLetters() {
        return letters;
    }

    public void setLetters(List<LetterTO> letters) {
        this.letters = letters;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getImage_name() {
        return image_name;
    }

    public void setImage_name(String image_name) {
        this.image_name = image_name;
    }

}
