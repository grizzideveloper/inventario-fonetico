package rizzi.graziano.inventariofonetico.view;

import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.LinearLayout;

public class BaseTestPage extends LinearLayout {

    public BaseTestPage(Context context) {
        super(context);
    }

    public BaseTestPage(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public BaseTestPage(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void updateUI(){

    }

    public void releaseBitmap(){

    }

    public void reloadBitmap(){
    }
}
