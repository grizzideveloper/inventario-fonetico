package rizzi.graziano.inventariofonetico.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import rizzi.graziano.inventariofonetico.R;

public class CellView extends LinearLayout {

    protected LayoutInflater inflater;
    protected Context context;
    protected View subroot,problemroot;
    protected TextView textView,sostTextView;
    protected LinearLayout root;

    public CellView(Context context, AttributeSet attrs){
        super(context, attrs);
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        init(false);
        this.context = context;
    }

    public CellView(Context context){
        super(context);
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        init(false);
        this.context = context;
    }

    public CellView(Context context, AttributeSet attrs,boolean keyboard){
        super(context, attrs);
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        init(keyboard);
        this.context = context;
    }

    public CellView(Context context,boolean keyboard){
        super(context);
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        init(keyboard);
        this.context = context;
    }

    public static enum State{
        NORM,
        SOST,
        PROB
    }

    public void setSostituition(String sostituition,State state){
        sostTextView.setText(sostituition);
        setState(state);
    }

    public void setState(State state){
        switch (state){
            case NORM:
                subroot.setBackgroundResource(R.drawable.letter_cell_shape);
                problemroot.setVisibility(GONE);
                break;
            case PROB:
                subroot.setBackgroundResource(R.drawable.letter_cell_shape_sostitution);
                problemroot.setVisibility(VISIBLE);
                break;
            case SOST:
                subroot.setBackgroundResource(R.drawable.letter_cell_shape_prob);
                problemroot.setVisibility(VISIBLE);
                break;
        }
    }

    public void setText(String text){
        /*if(text.length() > 1){
            subroot.getHeight();
            ViewGroup.LayoutParams params = root.getLayoutParams();
            params.width = (int)(params.height * 1.5);
            subroot.setLayoutParams(params);
        }*/
        textView.setText(text);
    }

    protected void init(boolean keyboard){
        if(!keyboard)inflater.inflate(R.layout.letter_cell, this, true);
        else inflater.inflate(R.layout.letter_cell_keyboard, this, true);
        subroot = this.findViewById(R.id.lettercell);
        textView = this.findViewById(R.id.cell_text);
        root = this.findViewById(R.id.cell_root);
        problemroot = this.findViewById(R.id.lettercell2);
        sostTextView = this.findViewById(R.id.cell_text2);
    }
}
