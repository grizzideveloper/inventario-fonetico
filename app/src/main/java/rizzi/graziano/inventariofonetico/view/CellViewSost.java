package rizzi.graziano.inventariofonetico.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.TextView;

import rizzi.graziano.inventariofonetico.R;

public class CellViewSost extends CellView {

    protected TextView sostCellText;

    public CellViewSost(Context context, AttributeSet attrs){
        super(context, attrs);
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.letter_cell_sost, this, true);
        subroot = this.findViewById(R.id.lettercell);
        textView = this.findViewById(R.id.cell_text);
        sostCellText = this.findViewById(R.id.sostcelltext);
        this.context = context;
    }

    public CellViewSost(Context context){
        super(context);
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.letter_cell_sost, this, true);
        subroot = this.findViewById(R.id.lettercell);
        textView = this.findViewById(R.id.cell_text);
        sostCellText = this.findViewById(R.id.sostcelltext);
        this.context = context;
    }

    public void setSostCellText(String text){
        sostCellText.setText(text);
    }

    @Override
    protected void init(boolean keyboard) {

    }
}
