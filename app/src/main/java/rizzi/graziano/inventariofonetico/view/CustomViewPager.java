package rizzi.graziano.inventariofonetico.view;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

public class CustomViewPager extends ViewPager {

    private boolean swipeActive = false;

    public CustomViewPager(Context context, AttributeSet attrs){
        super(context,attrs);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
        // Never allow swiping to switch between pages
        return swipeActive ? false:super.onInterceptTouchEvent(event);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        // Never allow swiping to switch between pages
        return swipeActive ? false:super.onTouchEvent(event);
    }

    public void enableSwipe(){
        swipeActive = false;
    }

    public void disableSwipe(){
        swipeActive = true;
    }
}
