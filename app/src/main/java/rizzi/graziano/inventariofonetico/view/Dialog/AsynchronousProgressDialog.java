package rizzi.graziano.inventariofonetico.view.Dialog;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;

import static rizzi.graziano.inventariofonetico.controller.ApplicationController.getContext;

public class AsynchronousProgressDialog {

    public AsynchronousProgressDialog(Context context) {
        this.context = context;
    }

    private Context context;
    private ProgressDialog dialog;

    public void showProgress(String titolo,String messaggio,boolean indeterminato){
        runOnUI(()-> dialog = ProgressDialog.show(context, titolo, messaggio, indeterminato));
    }

    public void showProgress(String titolo,String messaggio){
        showProgress(titolo,messaggio,true);
    }


    public void showProgress(){
        showProgress("Caricamento","Caricamento dati...",true);
    }

    public void stopProgress(){
        runOnUI(()->{
            if(dialog != null){
                dialog.dismiss();
            }
        });
    }

    private void runOnUI(Runnable runnable){
        new Handler(Looper.getMainLooper()).post(runnable);
    }

}
