package rizzi.graziano.inventariofonetico.view.Dialog;

import android.app.AlertDialog;
import android.content.Context;

public class CustomDialog extends AlertDialog {

    public CustomDialog(Context context) {
        super(context);
    }

    public CustomDialog(Context context, boolean cancelable, OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
    }

    public CustomDialog(Context context, int themeResId) {
        super(context, themeResId);
    }


}
