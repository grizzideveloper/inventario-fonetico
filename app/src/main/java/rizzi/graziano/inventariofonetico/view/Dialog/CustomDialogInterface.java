package rizzi.graziano.inventariofonetico.view.Dialog;

import android.content.DialogInterface;

public interface CustomDialogInterface {
    void onCancel(DialogInterface dialog);
    void onConfirm(DialogInterface dialog);
}
