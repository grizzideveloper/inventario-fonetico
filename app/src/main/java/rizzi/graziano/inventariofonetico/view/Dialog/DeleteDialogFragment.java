package rizzi.graziano.inventariofonetico.view.Dialog;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;


public class DeleteDialogFragment extends DialogFragment {

    private CustomDialogInterface customDialogInterface;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("Sei sicuro di voler cancellare la scheda?")
                .setPositiveButton("Si", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // FIRE ZE MISSILES!
                        customDialogInterface.onConfirm(dialog);
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog
                        customDialogInterface.onCancel(dialog);
                    }
                });
        // Create the AlertDialog object and return it
        return builder.create();
    }

    public void setCustomDialogInterface(CustomDialogInterface customDialogInterface) {
        this.customDialogInterface = customDialogInterface;
    }
}