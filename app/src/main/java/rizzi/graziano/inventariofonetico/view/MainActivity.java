package rizzi.graziano.inventariofonetico.view;

import android.widget.ListView;

import rizzi.graziano.inventariofonetico.R;
import rizzi.graziano.inventariofonetico.controller.MainActivityController;

public class MainActivity extends MainActivityController{


    @Override
    protected void onCreateView() {
        setContentView(R.layout.activity_main);
        ListView listView = findViewById(R.id.testList);
        listView.setAdapter(getTestListAdapter());
    }


}
