package rizzi.graziano.inventariofonetico.view;


import android.content.Context;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;

import rizzi.graziano.inventariofonetico.R;
import rizzi.graziano.inventariofonetico.controller.adapter.CustomTableKeyboardAdapter;

public class MyKeyboardView extends LinearLayout {

    private Context context;
    private LayoutInflater inflater;
    private TableLayout tableLayout;
    private Button annullaButton,cancellaButton;
    private Runnable runnable;

    private CustomTableKeyboardAdapter pageAdapter;

    public MyKeyboardView(Context context, AttributeSet attributeSet){
        super(context,attributeSet);
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if(inflater!=null){
            inflater.inflate(R.layout.test_page, this, true);
            this.tableLayout = findViewById(R.id.table);
            this.cancellaButton = findViewById(R.id.cancella_button);
            cancellaButton.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(pageAdapter != null)pageAdapter.onClearButtonClick();
                    if(runnable != null) runnable.run();
                }
            });
            this.annullaButton = findViewById(R.id.annulla_button);
            annullaButton.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(pageAdapter != null)pageAdapter.onCancelingButton();
                }
            });
        }
        this.context = context;
    }

    public void setAdapter(CustomTableKeyboardAdapter customTableKeyboardAdapter){
        this.pageAdapter = customTableKeyboardAdapter;
        drow();
    }

    private void drow(){
        if(pageAdapter != null){
            int rowCount = pageAdapter.getRowCount();
            for (int i = 0;i < rowCount; i++){
                TableRow tableRow = new TableRow(context);
                int columnCount = pageAdapter.getColumnCount(i);
                for(int j = 0; j < columnCount; j++){
                    CellView view = new CellView(context,true);

                    //ID.25
                    /*if(!pageAdapter.getStyle(i,j).equals("")){
                        view = new CellViewSost(context);
                        ((CellViewSost)view).setSostCellText(pageAdapter.getStyle(i,j).toUpperCase());
                    }*/
                    final int col = j;
                    final int row = i;
                    view.setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            pageAdapter.notifyClick(row,col);
                            if(runnable != null) runnable.run();
                        }
                    });
                    tableRow.addView(view);
                    String text =  pageAdapter.getText(i,j);
                    view.setText(text.toLowerCase());
                }
                LayoutParams params = new TableRow.LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
                params.gravity = Gravity.CENTER;
                tableLayout.addView(tableRow);
                tableRow.setLayoutParams(params);
            }
            TableRow tableRow = new TableRow(context);

            LayoutParams params = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,50);
            params.gravity = Gravity.CENTER;
            tableLayout.addView(tableRow);
            tableRow.setLayoutParams(params);
        }
    }

    public void show(Runnable runnable){
        this.runnable = runnable;
        this.setVisibility(VISIBLE);
    }

    public void hide(){
        this.setVisibility(GONE);
    }

}
