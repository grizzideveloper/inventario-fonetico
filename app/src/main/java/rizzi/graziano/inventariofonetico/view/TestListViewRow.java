package rizzi.graziano.inventariofonetico.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import rizzi.graziano.inventariofonetico.R;

public abstract class TestListViewRow implements View.OnClickListener{

    private View row;
    private Context context;
    private int position;


    private TextView title;
    private TextView desc;
    private TextView level;



    public TestListViewRow(Context context, ViewGroup viewGroup,int position){
        this.context = context;
        this.position = position;
        this.row = LayoutInflater.from(context).
                inflate(R.layout.list_test_row, viewGroup, false);
        this.title = row.findViewById(R.id.test_title);
        this.desc = row.findViewById(R.id.test_desc);
        this.level = row.findViewById(R.id.test_level);
        this.row.setOnClickListener(this);
    }

    public void setTitle(String title){
        this.title.setText(title);
    }

    public void setDesc(String desc){
        this.desc.setText(desc);
    }

    public void setLevel(int level){
        this.level.setText("LVL: " + level);
    }

    public View getRow(){
        return row;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

}
