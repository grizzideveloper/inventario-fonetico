package rizzi.graziano.inventariofonetico.view;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;

import rizzi.graziano.inventariofonetico.R;
import rizzi.graziano.inventariofonetico.controller.ApplicationController;
import rizzi.graziano.inventariofonetico.controller.adapter.CustomTableAdapter;
import rizzi.graziano.inventariofonetico.data.persistence.entity.Letter;
import rizzi.graziano.inventariofonetico.data.persistence.entity.Test;
import rizzi.graziano.inventariofonetico.data.to.LetterTO;
import rizzi.graziano.inventariofonetico.data.to.TestTO;
import rizzi.graziano.inventariofonetico.data.to.WordTO;


public abstract class TestPageResultView extends BaseTestPage {

    private Context context;
    private LayoutInflater inflater;
    private Button endTest;
    private TestTO test;
    private List<WordTO> errorWords = new LinkedList<>();
    private HashSet<LetterTO> errorLetters = new HashSet<>();

    public TestPageResultView(Context context, AttributeSet attrs) {
        super(context, attrs);
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.activity_table_page_result, this, true);
        this.context = context;
        endTest = findViewById(R.id.endButton);
        endTest.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                onEndTest();
            }
        });
    }

    public TestPageResultView(Context context,TestTO test){
        super(context);
        this.test = test;
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.activity_table_page_result, this, true);
        this.context = context;
        endTest = findViewById(R.id.endButton);
        endTest.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                onEndTest();
            }
        });
    }

    public void  setPageAdapter(CustomTableAdapter adapter){

    }


    public abstract void onEndTest();

    @Override
    public void updateUI() {
        super.updateUI();
        errorLetters.clear();
        errorWords.clear();
        if(test != null){
            for(int i = 0;i < test.getWords().size();i++){
                boolean result = false;
                WordTO word = test.getWords().get(i);
                for (int j = 0;j< word.getLetters().size();j++){
                    LetterTO letter = word.getLetters().get(j);
                    if(letter.getResult() != null){
                        //TODO
                        //aggiungere la lettera
                        errorLetters.add(letter);
                        result = true;
                    }
                }
                if(result){
                    //TODO
                    //Aggiungere la parola
                    errorWords.add(word);
                    System.out.print(word.getWordText());
                    for (int j = 0;j< word.getLetters().size();j++){
                        LetterTO letter = word.getLetters().get(j);
                        if(letter.getResult() != null){
                            //TODO
                            //aggiungere la lettera
                            String str = letter.getText() + " prob "
                                    + letter.getResult().getProblemID();
                            for(Letter lt : ApplicationController.instance.letters){
                                if(lt.getUid() == letter.getResult().getReplaceLetterID()){
                                    str = str + " con " + lt.getText();
                                    break;
                                }
                            }
                            System.out.println(str);
                        }
                    }
                }
            }
        }
    }


}
