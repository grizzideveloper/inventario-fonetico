package rizzi.graziano.inventariofonetico.view;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import java.util.LinkedList;
import java.util.List;

import rizzi.graziano.inventariofonetico.R;
import rizzi.graziano.inventariofonetico.controller.adapter.CustomTableAdapter;
import rizzi.graziano.inventariofonetico.controller.adapter.ProblemDialogAdapter;
import rizzi.graziano.inventariofonetico.data.DataManager;
import rizzi.graziano.inventariofonetico.data.DataRequestCallback;
import rizzi.graziano.inventariofonetico.data.persistence.entity.Problem;


public class TestPageView extends BaseTestPage {

    private TableLayout tableLayout;
    private CustomTableAdapter pageAdapter;
    private Context context;
    private LayoutInflater inflater;
    private ImageView imageView;


    public TestPageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.test_page, this, true);
        this.context = context;
        this.tableLayout = (TableLayout)getChildAt(0);

    }

    public TestPageView(Context context){
        super(context);
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.activity_table_page, this, true);
        this.context = context;
        this.tableLayout = findViewById(R.id.table);
        this.imageView = findViewById(R.id.imageView);
    }

    public void  setPageAdapter(CustomTableAdapter adapter){
        this.pageAdapter = adapter;
        drow();
    }

    public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) >= reqHeight
                    && (halfWidth / inSampleSize) >= reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    public static Bitmap decodeSampledBitmapFromResource(Resources res, int resId,
                                                         int reqWidth, int reqHeight) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(res, resId, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeResource(res, resId, options);
    }

    private int getImageIdFromName(String name){
        Resources res = getResources();
        Drawable drawable = null;
        try {
            int resID = res.getIdentifier(name  , "drawable", getContext().getPackageName());
            return resID;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 0;
    }


    private Drawable getImageFromName(String name){
        Resources res = getResources();
        Drawable drawable = null;
        try {
            int resID = res.getIdentifier(name  , "drawable", getContext().getPackageName());
            drawable = res.getDrawable(resID );
        }catch (Exception e){
            e.printStackTrace();
        }
        return drawable;
    }

    public  void drow(){
        dropAllRow();
        if(pageAdapter != null){
            int rowCount = pageAdapter.getRowCount();
            for (int i = 0;i < rowCount; i++){
                TableRow tableRow = new TableRow(context);
                int columnCount = pageAdapter.getColumnCount(i);
                for(int j = 0; j < columnCount; j++){
                    CellView view = new CellView(context);
                    if(pageAdapter.getStyle(i,j).equals("")){
                        view.setState(CellView.State.NORM);
                    }else if(!pageAdapter.getStyle(i,j).contains("PROB")){
                        view.setSostituition(pageAdapter.getStyle(i,j),CellView.State.SOST);
                    }else if(pageAdapter.getStyle(i,j).contains("PROB")){
                        view.setSostituition(pageAdapter.getStyle(i,j).replace("PROB ",""),CellView.State.PROB);
                    }
                    final int col = j;
                    final int row = i;
                    view.setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            pageAdapter.notifyClick(row,col);
                        }
                    });
                    view.setOnLongClickListener(new OnLongClickListener() {
                        @Override
                        public boolean onLongClick(View v) {
                            showDialogProblem(v,col,row);
                            return true;
                        }
                    });
                    tableRow.addView(view);
                    String text =  pageAdapter.getText(i,j);
                    view.setText(text.toLowerCase());
                }
                LayoutParams params = new TableRow.LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
                params.gravity = Gravity.CENTER;
                tableLayout.addView(tableRow);
                tableRow.setLayoutParams(params);
            }
        }
    }

    public void dropAllRow(){
        tableLayout.removeAllViews();
    }

    @Override
    public void releaseBitmap(){
        if(((BitmapDrawable)imageView.getDrawable()).getBitmap()!=null)
        {
            Bitmap bitmap = ((BitmapDrawable)imageView.getDrawable()).getBitmap();
            bitmap.recycle();
        }
    }

    @Override
    public void reloadBitmap(){
        imageView.setImageDrawable(getImageFromName(pageAdapter.getImageName()));
    }

    private void showDialogProblem(View v,int col,int row){
        DataManager dataManager = DataManager.getInstance();
        List<Problem> problems = new LinkedList<>();
        dataManager.getProblems(new DataRequestCallback<List<Problem>>() {
            @Override
            public void onSuccess(List<Problem> dataResult) {
                problems.addAll(dataResult);
                for (int i = 0;i < problems.size();i++)if(!problems.get(i).isDialog())problems.remove(i);
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle("Seleziona distorsione");
                ProblemDialogAdapter adapter = new ProblemDialogAdapter(getContext(),R.layout.problem_dialog_row_2,problems);
                builder.setAdapter(adapter, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        pageAdapter.notifyLongClick(row,col,which + 2);
                    }
                });
                ((Activity)getContext()).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        AlertDialog alert = builder.create();
                        alert.show();
                    }
                });
            }

            @Override
            public void onFail(Exception e) {
                e.printStackTrace();
            }
        });

    }
}
